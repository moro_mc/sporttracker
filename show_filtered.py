import sys
import tkinter as tk
from tkinter import ttk, messagebox
import customtkinter as ctk
from activities import DistanceActivity, GeneralActivity, History
from equipment import Equipment, Garage


# TODO memorize last filters?
# TODO recover from empty
class ShowFiltered:
    def __init__(self, items_list, garage: Garage, history: History):
        if len(items_list) > 0:
            if isinstance(items_list[0], Equipment):
                self.__mode = "equipment"
                self.__item_list = sorted(items_list, key=lambda x: x.get_id())
            else:
                self.__mode = "activities"
                self.__item_list = sorted(items_list, key=lambda x: x.get_date(), reverse=True)
        else:
            self.__mode = "empty"
            self.__item_list = []
        self.__garage = garage
        self.__history = history
        self.__item = None
        self.__out = {"command": "close", "argument": None}

        self.__full_height = 720
        self.__width = 720
        dims = str(self.__width) + "x" + str(self.__full_height) + "+10+10"

        ctk.set_appearance_mode("system")
        ctk.set_default_color_theme("green")
        self.__window = ctk.CTk()  # create window
        self.__window.geometry(dims)
        self.__window.resizable(False, False)
        self.__window.title("Sport Tracker: Navigate")
        # call the specified function, if the user clicks the red arrow
        self.__window.protocol("WM_DELETE_WINDOW", self.__close)

        self.__bottom_frame = ctk.CTkFrame(self.__window)
        self.__separator = ttk.Separator(self.__bottom_frame, orient="horizontal")
        self.__back_to_menu_button = ctk.CTkButton(self.__bottom_frame, text="Menu", font=("Arial", 20), height=2,
                                                   command=self.__back_to_menu)
        self.__exit_button = ctk.CTkButton(self.__bottom_frame, text="Exit", font=("Arial", 20), height=2,
                                           command=self.__close)

        self.__sort_frame = ctk.CTkFrame(self.__window)
        self.__separator2 = ttk.Separator(self.__sort_frame, orient="horizontal")
        self.__sort_label = ctk.CTkLabel(self.__sort_frame, text="Sort by:", font=("Arial", 16))
        if self.__mode == "equipment":
            self.__sort_modes = ["ID", "Type", "Usage"]
        else:
            self.__sort_modes = ["Ascending Date", "Descending Date", "Type", "Shortest Duration", "Longest Duration"]
        self.__sort_combo = ctk.CTkComboBox(self.__sort_frame, values=self.__sort_modes, font=("Arial", 18), width=200,
                                            command=self.__sort)
        self.__filter_button = ctk.CTkButton(self.__sort_frame, text="Apply Filter", font=("Arial", 20), height=2,
                                             command=self.__filter)
        self.__remove_filter_button = ctk.CTkButton(self.__sort_frame, text="Remove Filter", font=("Arial", 20),
                                                    height=2,
                                                    command=self.__remove_filter)

        self.__list_frame = ctk.CTkFrame(self.__window)
        self.__menu_prompt = ctk.CTkLabel(self.__list_frame, text="Navigate", font=("Arial", 24))
        self.__error_label = ctk.CTkLabel(self.__list_frame, text="Select activity to proceed", font=("Arial", 16),
                                          text_color="red")
        self.__scroll_frame = ctk.CTkScrollableFrame(self.__list_frame)
        self.__buttons_list = []
        for i, item in enumerate(self.__item_list):
            text = self.__create_string(item)
            btn = ctk.CTkButton(self.__scroll_frame, text=text, font=("Arial", 16),
                                command=lambda x=i: self.__selected(x))
            self.__buttons_list.append(btn)

        self.__action_frame = ctk.CTkFrame(self.__window)
        self.__separator1 = ttk.Separator(self.__action_frame, orient="horizontal")
        self.__back_button = ctk.CTkButton(self.__action_frame, text="Back", font=("Arial", 20), height=2,
                                           command=self.__back)
        self.__modify_button = ctk.CTkButton(self.__action_frame, text="Modify", font=("Arial", 20), height=2,
                                             command=self.__modify)
        self.__delete_button = ctk.CTkButton(self.__action_frame, text="Delete", font=("Arial", 20), height=2,
                                             command=self.__delete)

        self.__show_frame = ctk.CTkFrame(self.__window)
        self.__item_type_label = ctk.CTkLabel(self.__show_frame, text="", font=("Arial", 24))
        self.__date_label = ctk.CTkLabel(self.__show_frame, text="", font=("Arial", 20))
        self.__distance_label = ctk.CTkLabel(self.__show_frame, text="", font=("Arial", 18))
        self.__duration_label = ctk.CTkLabel(self.__show_frame, text="", font=("Arial", 18))
        self.__pace_label = ctk.CTkLabel(self.__show_frame, text="", font=("Arial", 18))
        self.__altitude_label = ctk.CTkLabel(self.__show_frame, text="", font=("Arial", 16))
        self.__gain_label = ctk.CTkLabel(self.__show_frame, text="", font=("Arial", 16))
        self.__fatigue_label = ctk.CTkLabel(self.__show_frame, text="", font=("Arial", 16))
        self.__equipment_label = ctk.CTkLabel(self.__show_frame, text="", font=("Arial", 16))
        self.__conditions_label = ctk.CTkLabel(self.__show_frame, text="", font=("Arial", 16))
        self.__notes_label = ctk.CTkLabel(self.__show_frame, text="", font=("Arial", 16))
        self.__usage_label = ctk.CTkLabel(self.__show_frame, text="", font=("Arial", 18))

        self.__show_labels = [self.__item_type_label, self.__date_label, self.__distance_label, self.__duration_label,
                              self.__pace_label, self.__altitude_label, self.__gain_label, self.__fatigue_label,
                              self.__equipment_label, self.__conditions_label, self.__notes_label, self.__usage_label]

    def run(self):
        self.__bottom_frame.pack(side="bottom", expand=False, fill="x")
        self.__separator.pack(side="top", expand=False, fill="x")
        self.__back_to_menu_button.pack(side="left", padx=20, pady=20, expand=False)
        self.__exit_button.pack(side="right", padx=20, pady=20, expand=False)

        self.__separator2.pack(side="top", expand=False, fill="x")
        self.__sort_label.pack(side="left", expand=False, padx=20, pady=20)
        self.__sort_combo.pack(side="left", expand=False, padx=20, pady=20)
        self.__filter_button.pack(side="right", expand=False, padx=20, pady=20)
        self.__remove_filter_button.pack(side="right", expand=False, padx=20, pady=20)
        if self.__mode == "empty":
            self.__list_frame.pack(side="top", expand=True, fill="both")
            self.__menu_prompt.configure(text="No item matches the filter!", font=("Arial", 24))
            self.__menu_prompt.pack(side="top", anchor="w", expand=False, padx=20, pady=10)
            self.__sort_frame.pack(side="bottom", expand=False, fill="x")
        else:
            self.__separator1.pack(side="top", expand=False, fill="x")
            self.__back_button.pack(side="left", expand=False, padx=20, pady=20)
            self.__modify_button.pack(side="left", expand=True, padx=20, pady=20)
            self.__delete_button.pack(side="right", expand=False, padx=20, pady=20)

            self.__assembly_list()
        self.__window.mainloop()
        return self.__out

    def __assembly_list(self):
        self.__show_frame.pack_forget()
        self.__action_frame.pack_forget()
        self.__list_frame.pack(side="top", expand=True, fill="both")
        self.__menu_prompt.pack(side="top", anchor="w", expand=False, padx=20, pady=10)
        self.__scroll_frame.pack(side="top", anchor="w", expand=True, padx=0, pady=10, fil="both")
        if not len(self.__item_list) == 0:
            for i in range(len(self.__buttons_list)):
                self.__buttons_list[i].pack(side="top", anchor="w", expand=False, fill="x", padx=20, pady=5)
        else:
            self.__menu_prompt.configure(text="No item matched the filters")
        self.__sort_frame.pack(side="bottom", expand=False, fill="x")

    def __assembly_show(self):
        self.__list_frame.pack_forget()
        self.__sort_frame.pack_forget()
        self.__show_frame.pack(side="top", expand=True, fill="both")
        self.__action_frame.pack(side="bottom", expand=False, fill="x")

        if self.__mode == "equipment":
            self.__item_type_label.configure(text=f"{self.__item.get_typology()} (ID: {self.__item.get_id()})")
            self.__item_type_label.pack(side="top", anchor="w", expand=False, padx=20, pady=20)
            self.__notes_label.configure(text=f"Notes: {self.__item.get_notes()}")
            if self.__item.get_usage() is None:
                self.__notes_label.pack(side="top", anchor="w", expand=False, padx=20, pady=5)
            else:
                txt = f"Usage: {self.__item.get_usage():.2f}km"
                if not self.__item.get_alive():
                    txt += f" (Not Usable)"

                self.__usage_label.configure(text=txt)
                self.__usage_label.pack(side="top", anchor="w", expand=False, padx=20, pady=5)
                self.__notes_label.pack(side="top", anchor="w", expand=False, padx=20, pady=5)

        else:
            self.__item_type_label.configure(
                text=f"{self.__item.get_item_type()}, of type {self.__item.get_typology()}")
            self.__item_type_label.pack(side="top", anchor="w", expand=False, padx=20, pady=20)
            self.__date_label.configure(text=f"Date: {self.__item.get_date()}")
            self.__date_label.pack(side="top", anchor="w", expand=False, padx=20, pady=5)
            if isinstance(self.__item, DistanceActivity):
                self.__distance_label.configure(text=f"Distance: {self.__item.get_distance():.02f}km")
                self.__distance_label.pack(side="top", anchor="w", expand=False, padx=20, pady=5)
            self.__duration_label.configure(text=f"Time: {self.__item.get_duration()}")
            self.__duration_label.pack(side="top", anchor="w", expand=False, padx=20, pady=5)
            if isinstance(self.__item, DistanceActivity):
                self.__pace_label.configure(text=f"Pace: {self.__item.get_pace():.02f}km/h "
                                                 f"({DistanceActivity.invert_pace(self.__item.get_pace())}/km)")
                self.__pace_label.pack(side="top", anchor="w", expand=False, padx=20, pady=5)
            if isinstance(self.__item, DistanceActivity):
                self.__altitude_label.configure(text=f"Altitude reached: {self.__item.get_max_altitude()}m")
                self.__altitude_label.pack(side="top", anchor="w", expand=False, padx=20, pady=5)
                self.__gain_label.configure(text=f"Gain: {self.__item.get_altitude_gain()}m gain")
                self.__gain_label.pack(side="top", anchor="w", expand=False, padx=20, pady=5)

            if self.__item.get_equipment() == 0:
                self.__equipment_label.configure(text="No Equipment")
                # self.__equipment_label.pack_forget()
            else:
                eq = [x for x in self.__garage.get_list() if x.get_id() == self.__item.get_equipment()]
                eq_txt = f"Equipment ID: {self.__item.get_equipment()} [ {eq[0].get_typology()}: {eq[0].get_notes()} ]"
                self.__equipment_label.configure(text=eq_txt)
                self.__equipment_label.pack(side="top", anchor="w", expand=False, padx=20, pady=5)
            self.__fatigue_label.configure(text=f"Fatigue perceived: {self.__item.get_fatigue()}")
            self.__fatigue_label.pack(side="top", anchor="w", expand=False, padx=20, pady=5)
            if isinstance(self.__item, DistanceActivity):
                txt = ""
                h, r = self.__item.get_conditions()
                if h or r:
                    if h:
                        txt += " Hot"
                    if h and r:
                        txt += " and"
                    if r:
                        txt += " Rainy"
                else:
                    txt = "Normal"
            else:
                if self.__item.get_conditions()[1]:
                    txt = "Indoors"
                else:
                    txt = "Outdoors"
                if self.__item.get_conditions()[0]:
                    txt += " and Hot"
            self.__conditions_label.configure(text=f"Conditions: {txt}")
            self.__conditions_label.pack(side="top", anchor="w", expand=False, padx=20, pady=5)

            self.__notes_label.configure(text=f"Notes: {self.__item.get_notes()}")
            self.__notes_label.pack(side="top", anchor="w", expand=False, padx=20, pady=5)

    @staticmethod
    def __create_string(item):
        if isinstance(item, DistanceActivity):
            text = f"{item.get_item_type()} ({item.get_typology()}): \t{item.get_date()}, {item.get_distance()}km in " \
                   f"{item.get_duration()} ({item.get_pace():.2f}km/h). "

        elif isinstance(item, GeneralActivity):
            text = f"{item.get_item_type()} ({item.get_typology()}): \t{item.get_date()}, for {item.get_duration()}. "

        elif isinstance(item, Equipment):
            text = f"{item.get_id()}: {item.get_typology()} ({item.get_notes()})."
            if item.get_usage() is not None:
                text += f" Usage: {item.get_usage():.2f}km"
            if item.get_alive():
                text += " (Usable)"
            else:
                text += " (Limit reached)"
        else:
            sys.exit("Wrong element passed to NAVIGATE")
        return text

    def __selected(self, index):
        self.__item = self.__item_list[index]
        self.__list_frame.pack_forget()
        self.__show_frame.pack(side="top", anchor="w", expand=True, fill="both")
        self.__assembly_show()

    def __sort(self, mode):
        if self.__mode == "equipment":
            if mode == self.__sort_modes[0]:
                fcn = lambda x: x[0].get_id()
                reverse = False
            elif mode == self.__sort_modes[1]:
                fcn = lambda x: x[0].get_typology()
                reverse = False
            elif mode == self.__sort_modes[2]:
                fcn = lambda x: x[0].get_usage()
                reverse = True
            else:
                return
        else:
            if mode == self.__sort_modes[0]:
                fcn = lambda x: x[0].get_date()
                reverse = True
            elif mode == self.__sort_modes[1]:
                fcn = lambda x: x[0].get_date()
                reverse = False
            elif mode == self.__sort_modes[2]:
                fcn = lambda x: x[0].get_item_type()
                reverse = False
            elif mode == self.__sort_modes[3]:
                fcn = lambda x: x[0].get_duration()
                reverse = False
            elif mode == self.__sort_modes[4]:
                fcn = lambda x: x[0].get_duration()
                reverse = True
            else:
                return

        for btn in self.__buttons_list:
            btn.pack_forget()
        self.__item_list, self.__buttons_list = zip(*sorted(zip(self.__item_list, self.__buttons_list), key=fcn,
                                                            reverse=reverse))
        for btn in self.__buttons_list:
            btn.pack(side="top", anchor="w", expand=False, fill="x", padx=20, pady=5)

    def __back_to_menu(self):
        self.__window.destroy()
        self.__out = {"command": "home", "argument": None}

    def __filter(self):
        self.__window.destroy()
        if self.__mode == "equipment":
            self.__out = {"command": "filter_equipment", "argument": None}
        else:
            self.__out = {"command": "filter_activities", "argument": None}

    def __remove_filter(self):
        if self.__mode == "empty":
            return
        if self.__mode == "equipment":
            self.__item_list = sorted(self.__garage.get_list(), key=lambda x: x.get_id())
        elif self.__mode == "activities":
            self.__item_list = sorted(self.__history.get_list(), key=lambda x: x.get_date(), reverse=True)
        else:
            return

        for btn in self.__buttons_list:
            btn.pack_forget()

        self.__buttons_list = []
        for i, item in enumerate(self.__item_list):
            text = self.__create_string(item)
            btn = ctk.CTkButton(self.__scroll_frame, text=text, font=("Arial", 16),
                                command=lambda x=i: self.__selected(x))
            self.__buttons_list.append(btn)
        self.__assembly_list()
        self.__sort(self.__sort_modes[0])

    def __back(self):
        # TODO: if on list, back to filter (or create a filter button that calls the function with gui), else back to the list
        self.__item = None
        self.__show_frame.pack_forget()
        for label in self.__show_labels:
            label.pack_forget()
        self.__list_frame.pack(side="top", anchor="w", expand=True, fill="both")
        self.__assembly_list()

    def __modify(self):
        self.__out = {"command": "modify", "argument": self.__item}
        self.__window.destroy()

    def __delete(self):
        ans = tk.messagebox.askokcancel(title="Delete",
                                        message="Are you sure you want to delete this item?")  # create messagebox
        if ans:
            self.__out = {"command": "delete", "argument": self.__item}
            self.__window.destroy()
        else:
            pass

    def __close(self):
        ans = tk.messagebox.askyesno(title="Exit", message="Are you sure you want to quit?")  # create messagebox
        if ans:  # if yes, close
            self.__window.destroy()  # close the home window
            self.__out = {"command": "close", "argument": None}
        else:  # if no, ignore
            pass
