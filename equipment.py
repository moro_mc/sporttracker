import sys
from activities import History, DistanceActivity
import pandas as pd
import numpy as np


class Garage:
    def __init__(self, files):
        self.__file = [files[0], "equipment"]  # filename and sheet_name
        self.__backup_file = [files[1], "equipment"]  # filename and sheet_name
        self.__stats = {}
        self.__backup(download=True)
        self.__equipment_list = self.__read()
        self.__update_stats()

    def __read(self):
        filename = self.get_file()
        items = []
        reader = pd.read_excel(filename[0], sheet_name=filename[1])
        for _, row in reader.iterrows():
            item = Equipment(row)
            items.append(item)
        return items

    def write(self):
        filename = self.get_file()
        file = []
        for item in self.get_list():
            ans = item.write()
            file.append(ans)
        file = pd.DataFrame(file)
        with pd.ExcelWriter(filename[0], mode="a", if_sheet_exists="replace") as writer:
            file.to_excel(writer, sheet_name=filename[1], index=False)
        self.__backup(download=False)

    def __backup(self, download):
        """ download is True if we want to restore the data from the backup file to the actual database
        download is False if we are storing the updated data to the backup, after a successful writing operation"""
        if download:
            source = self.get_backup_file()
            target = self.get_file()
        else:
            source = self.get_file()
            target = self.get_backup_file()

        file = pd.read_excel(source[0], sheet_name=source[1])
        with pd.ExcelWriter(target[0], mode="a", if_sheet_exists="replace") as writer:
            file.to_excel(writer, sheet_name=target[1], index=False)

    def get_list(self):
        return self.__equipment_list

    def get_file(self):
        return self.__file

    def get_backup_file(self):
        return self.__backup_file

    def get_stats(self):
        return self.__stats

    def add_item(self, item):
        self.__equipment_list.append(item)

    def remove_item(self, item):
        for i, equipment in enumerate(self.__equipment_list):
            if equipment == item:
                self.__equipment_list.pop(i)
                break
        self.__update_stats()

    def update_usage(self, history: History):
        for item in self.get_list():
            if item.get_typology() in Equipment.get_consumable():
                usage = self.read_usage(item.get_id(), history)
                item.set_usage(usage)

    def __update_stats(self):
        num = len(self.get_list())
        items = self.get_list()
        available = Equipment.get_available()
        counter = np.zeros(shape=[num+1, ])
        usable = 0
        unusable = 0
        for item in items:
            if item.get_typology() in Equipment.get_consumable():
                if item.get_alive():
                    usable += 1
                else:
                    unusable += 1
            idx = available.index(item.get_typology())
            counter[idx] += 1
        how_many = int(max(counter))
        most_frequent = available[np.argmax(counter)]
        string = f"{num} items ({usable} are usable, {unusable} are not)\nMost frequent: {most_frequent} ({how_many})\n"
        self.__stats = {"num": num, "typologies": counter, "most_frequent": most_frequent,
                        "most_frequent_num": how_many, "usable": usable, "unusable": unusable}
        print(string)

    @staticmethod
    def read_usage(number: int, history: History):
        usage = 0
        for activity in history.get_list():
            if isinstance(activity, DistanceActivity):
                if activity.get_equipment() == number:
                    usage += activity.get_distance()
        return usage


class Equipment:
    def __init__(self, sequence):
        self.__id = None
        self.__typology = None
        self.__usage = None
        self.__alive = None
        self.__notes = None
        self.__consumable_items_list = ["Running Shoes", "Walking Shoes", "Hiking Boots", "Road Bike", "Mountain Bike"]
        self.__available_typologies_list = ["None", "Running Shoes", "Walking Shoes", "Hiking Boots", "Mountain Bike",
                                            "Road Bike", "Skis", "Football Shoes"]
        # if adding new available, remember to propagate in the static method and in the activities
        self.__read(sequence)

    def __read(self, sequence):
        self.__id = int(sequence["ID"])
        self.__typology = sequence["typology"]
        self.__alive = sequence["alive"]
        self.__notes = sequence["notes"]

    def write(self):
        num = self.__id
        typology = self.__typology
        alive = self.__alive
        notes = self.__notes
        if isinstance(num, int) and typology in self.__available_typologies_list and alive in [True, False]:
            ans = {"ID": num, "typology": typology, "alive": alive, "notes": notes}
        else:
            sys.exit("Error while writing")
        return ans

    def __str__(self):
        return f"{self.__typology}: ID={self.__id}\n" \
               f"Usage: {self.__usage} -> Usable: {self.__alive}\n" \
               f"Notes:{self.__notes}\n"

    def get_usage(self):
        return self.__usage

    def set_usage(self, usage):
        self.__usage = usage

    def get_alive(self):
        return self.__alive

    def get_id(self):
        return self.__id

    def get_typology(self):
        return self.__typology

    def get_notes(self):
        return self.__notes

    @staticmethod
    def get_available():
        return ["None", "Running Shoes", "Walking Shoes", "Hiking Boots", "Mountain Bike", "Road Bike", "Skis",
                "Football Shoes"]

    @staticmethod
    def get_consumable():
        return ["Running Shoes", "Walking Shoes", "Hiking Boots", "Road Bike", "Mountain Bike"]
