import tkinter as tk
from tkinter import messagebox, ttk
import customtkinter as ctk
import sys


class MenuGUI:
    def __init__(self, garage_list, activities_list):
        self.__out = {"command": "close", "argument": None}
        self.__garage_list = garage_list
        self.__activities_list = activities_list
        self.__available_activities = ["Run", "Hike", "Bike", "Gym", "Sport"]

        # ---- initialize window, adjust its parameters ----
        ctk.set_appearance_mode("system")
        ctk.set_default_color_theme("blue")
        self.__window = ctk.CTk()  # create window
        self.__window.geometry("600x400+10+10")
        self.__window.resizable(False, False)
        self.__window.title("Sport Tracker: Main Menu")
        # call the specified function, if the user clicks the red arrow
        self.__window.protocol("WM_DELETE_WINDOW", self.__close)

        self.__bottom_frame = ctk.CTkFrame(self.__window)
        self.__back_to_menu_button = ctk.CTkButton(self.__bottom_frame, text="Menu",  font=("Arial", 20), height=2,
                                                   command=self.__back_to_menu)
        self.__exit_button = ctk.CTkButton(self.__bottom_frame, text="Exit",  font=("Arial", 20), height=2,
                                           command=self.__close)

        # ---- initialize the menu page, with its labels, and the buttons to go to the other pages ----
        self.__menu_page = ctk.CTkFrame(self.__window)
        self.__menu_title = ctk.CTkLabel(self.__menu_page, text="Welcome to Your Sport Tracker", font=("Arial", 24))
        self.__menu_prompt = ctk.CTkLabel(self.__menu_page, text="What Would You Like To Do?", font=("Arial", 20))

        self.__add_button = ctk.CTkButton(self.__menu_page, text="New Item",  font=("Arial", 20), height=2,
                                          command=self.__add_item_page)
        self.__garage_button = ctk.CTkButton(self.__menu_page, text="Garage", font=("Arial", 20), height=2,
                                             command=self.__navigate_equipment)
        self.__history_button = ctk.CTkButton(self.__menu_page, text="Activities", font=("Arial", 20), height=2,
                                              command=self.__navigate_activities)
        self.__statistics_button = ctk.CTkButton(self.__menu_page, text="Statistics", font=("Arial", 20), height=2,
                                                 command=self.__statistics_page)
        self.__visualize_button = ctk.CTkButton(self.__menu_page, text="Visualize", font=("Arial", 20), height=2,
                                                command=self.__visualize_page)

        # ttk.Style().configure('TSeparator', background="grey")
        self.__separator1 = ttk.Separator(self.__menu_page, orient="horizontal")
        self.__separator2 = ttk.Separator(self.__menu_page, orient="horizontal")
        self.__separator3 = ttk.Separator(self.__menu_page, orient="horizontal")
        self.__bottom_separator = ttk.Separator(self.__bottom_frame, orient="horizontal")

        self.__add_frame = ctk.CTkFrame(self.__window)
        self.__what_prompt = ctk.CTkLabel(self.__add_frame, text="What Would You Like To Add?", font=("Arial", 20))
        available_add = self.__available_activities
        available_add.append("Equipment")
        self.__what_buttons = []
        for i, what in enumerate(available_add):
            btn = ctk.CTkButton(self.__add_frame, text=what, font=("Arial", 20), height=2,
                                command=lambda x=what: self.__add_item(x))
            self.__what_buttons.append(btn)
        self.__separator4 = ttk.Separator(self.__add_frame, orient="horizontal")
        self.__separator5 = ttk.Separator(self.__add_frame, orient="horizontal")
        self.__separator6 = ttk.Separator(self.__add_frame, orient="horizontal")

        self.__statistics_frame = ctk.CTkFrame(self.__window)
        self.__statistics_prompt = ctk.CTkLabel(self.__statistics_frame, text="Select Statistic", font=("Arial", 20))

        self.__visualize_frame = ctk.CTkFrame(self.__window)
        self.__visualize_prompt = ctk.CTkLabel(self.__visualize_frame, text="Select Visualization", font=("Arial", 20))

        self.__menu_page.columnconfigure(index=0, weight=1)
        self.__menu_page.columnconfigure(index=1, weight=1)

        self.__add_frame.columnconfigure(index=0, weight=1)
        self.__add_frame.columnconfigure(index=1, weight=1)
        self.__add_frame.columnconfigure(index=2, weight=1)

    def run(self):
        self.__menu_page.pack(side="top", expand=True, fill="both")
        self.__bottom_frame.pack(side="bottom", expand=False, fill="x")
        self.__assemble()
        self.__window.mainloop()
        return self.__out

    def __assemble(self):
        self.__bottom_separator.pack(side="top", expand=False, fill="x")
        self.__back_to_menu_button.pack(side="left", expand=False, padx=20, pady=20)
        self.__exit_button.pack(side="right", expand=False, padx=20, pady=20)

        self.__menu_title.grid(row=0, column=0, sticky="w", padx=20, pady=20)
        self.__menu_prompt.grid(row=1, column=0, sticky="w", padx=20, pady=0)
        self.__separator1.grid(row=2, column=0, sticky="ew", pady=10, columnspan=2)
        self.__garage_button.grid(row=3, column=0, sticky="w", padx=20, pady=10)
        self.__history_button.grid(row=3, column=1, sticky="e", padx=20, pady=10)
        self.__separator2.grid(row=4, column=0, sticky="ew", pady=10, columnspan=2)
        self.__add_button.grid(row=5, column=0, sticky="w", padx=20, pady=10)
        self.__separator3.grid(row=6, column=0, sticky="ew", pady=10, columnspan=2)
        self.__statistics_button.grid(row=7, column=0, sticky="w", padx=20, pady=10)
        self.__visualize_button.grid(row=7, column=1, sticky="e", padx=20, pady=10)

        self.__what_prompt.grid(row=0, column=0, sticky="nw", padx=20, pady=20, columnspan=3)
        self.__separator4.grid(row=1, column=0, sticky="ew", pady=10, columnspan=3)
        self.__what_buttons[5].grid(row=2, column=0, padx=20, pady=10, sticky="w")
        self.__separator5.grid(row=3, column=0, sticky="ew", pady=10, columnspan=3)
        self.__what_buttons[0].grid(row=4, column=0, padx=20, pady=10, sticky="w")
        self.__what_buttons[1].grid(row=4, column=1, padx=20, pady=10, sticky="w")
        self.__what_buttons[2].grid(row=4, column=2, padx=20, pady=10, sticky="w")
        self.__separator6.grid(row=5, column=0, sticky="ew", pady=10, columnspan=3)
        self.__what_buttons[3].grid(row=6, column=0, padx=20, pady=10, sticky="w")
        self.__what_buttons[4].grid(row=6, column=1, padx=20, pady=10, sticky="w")

        self.__statistics_prompt.pack(side="top", anchor="nw", expand=False, padx=20, pady=20)
        self.__visualize_prompt.pack(side="top", anchor="nw", expand=False, padx=20, pady=20)

    def __close(self):
        ans = tk.messagebox.askyesno(title="Exit", message="Are you sure you want to quit?")  # create messagebox
        if ans:  # if yes, close
            self.__window.destroy()  # close the home window
            self.__out = {"command": "close", "argument": None}
        else:  # if no, ignore
            pass
        # self.window.destroy()

    def __back_to_menu(self):
        """ clean the current page, place the new onw """
        # clean, regardless of the current page
        self.__add_frame.pack_forget()
        self.__visualize_frame.pack_forget()
        self.__statistics_frame.pack_forget()
        # place the menu page
        self.__menu_page.pack(side="top", expand=True, fill= "both")

    def __add_item_page(self):
        self.__menu_page.pack_forget()
        self.__add_frame.pack(side="top", expand=True, fill="both")

    def __navigate_equipment(self):
        self.__window.destroy()
        self.__out = {"command": "navigate", "argument": self.__garage_list}

    def __navigate_activities(self):
        self.__window.destroy()
        self.__out = {"command": "navigate", "argument": self.__activities_list}

    def __statistics_page(self):
        self.__menu_page.pack_forget()
        self.__statistics_frame.pack(side="top", expand=True, fill="both")

    def __visualize_page(self):
        self.__menu_page.pack_forget()
        self.__visualize_frame.pack(side="top", expand=True, fill="both")

    def __add_item(self, typology):
        # typology = self.__what_combobox.get()
        self.__window.destroy()
        self.__out = {"command": "add", "argument": typology}

    def __visualize(self, mode):  # todo: every button leads to this function: use a lambda function to pass the argument
        self.__window.destroy()
        self.__out = {"command": "visualize", "argument": mode}

    def __statistics(self, mode):  # todo: every button leads to this function: use a lambda function to pass the argument
        self.__window.destroy()
        self.__out = {"command": "statistics", "argument": mode}
