import datetime
import sys
import tkinter as tk
from tkinter import ttk, messagebox
import customtkinter as ctk
from equipment import Equipment, Garage
from activities import History, DistanceActivity, GeneralActivity
from utils import *


def filter_activities(items_list, mask):
    if mask["item_type"] is not None:
        items_list = filter(lambda x: x.get_item_type() in mask["item_type"], items_list)

    if mask["min_date"] is not None:
        items_list = filter(lambda x: mask["min_date"] <= x.get_date() <= mask["max_date"], items_list)

    if mask["min_distance"] is not None:
        items_list = filter(lambda x: mask["min_distance"] <= x.get_distance() <= mask["max_distance"], items_list)

    if mask["min_duration"] is not None:
        items_list = filter(lambda x: mask["min_duration"] <= x.get_duration() <= mask["max_duration"], items_list)

    if mask["min_pace"] is not None:
        items_list = filter(lambda x: mask["min_pace"] <= x.get_pace() <= mask["max_pace"], items_list)

    if mask["min_fatigue"] is not None:
        items_list = filter(lambda x: mask["min_fatigue"] <= x.get_fatigue() <= mask["max_fatigue"], items_list)

    if mask["min_altitude_gain"] is not None:
        items_list = filter(lambda x: mask["min_altitude_gain"] <= x.get_altitude_gain() <= mask["max_altitude_gain"],
                            items_list)

    if mask["min_max_altitude"] is not None:
        items_list = filter(lambda x: mask["min_max_altitude"] <= x.get_max_altitude() <= mask["max_max_altitude"],
                            items_list)

    if mask["equipment"] is not None:
        items_list = filter(lambda x: x.get_equipment() in mask["equipment"], items_list)

    if mask["typology"] is not None:
        items_list = filter(lambda x: x.get_typology() in mask["typology"], items_list)

    if mask["hot"] is not None:
        items_list = filter(lambda x: x.get_conditions()[0] == mask["hot"], items_list)

    if mask["equipment"] is not None:
        items_list = filter(lambda x: x.get_conditions()[1] == mask["rain"], items_list)

    if mask["indoor"] is not None:
        items_list = filter(lambda x: x.get_conditions()[1] == mask["indoor"], items_list)

    return list(items_list)


class FilterActivities:

    # TODO: if you select an activity type, slide down the other activities, show checklists with the inner typology;
    #  default=selected
    def __init__(self, history: History):
        self.__history = history
        self.__out = {"command": "close", "argument": None}
        self.__min_date = None
        self.__max_date = None
        self.__min_duration = None
        self.__max_duration = None
        self.__min_fatigue = None
        self.__max_fatigue = None
        self.__min_distance = None
        self.__max_distance = None
        self.__min_max_altitude = None
        self.__max_max_altitude = None
        self.__min_altitude_gain = None
        self.__max_altitude_gain = None
        self.__hot = None
        self.__rain = None
        self.__min_pace = None
        self.__max_pace = None

        self.__indoor = None
        self.__available_activities_list = ["Run", "Hike", "Bike", "Gym", "Sport"]

        self.__full_height = 720
        self.__width = 1000
        dims = str(self.__width) + "x" + str(self.__full_height) + "+10+10"

        ctk.set_appearance_mode("system")
        ctk.set_default_color_theme("green")
        self.__window = ctk.CTk()  # create window
        self.__window.geometry(dims)
        self.__window.resizable(False, False)
        self.__window.title("Sport Tracker: Navigate History")
        # call the specified function, if the user clicks the red arrow
        self.__window.protocol("WM_DELETE_WINDOW", self.__close)

        self.__bottom_frame = ctk.CTkFrame(self.__window, fg_color=self.__window._fg_color)
        self.__bottom_separator = ttk.Separator(self.__bottom_frame, orient="horizontal")
        self.__back_to_menu_button = ctk.CTkButton(self.__bottom_frame, text="Menu", font=("Arial", 20), height=2,
                                                   command=self.__back_to_menu)
        self.__exit_button = ctk.CTkButton(self.__bottom_frame, text="Exit", font=("Arial", 20), height=2,
                                           command=self.__close)

        self.__action_frame = ctk.CTkFrame(self.__window, fg_color=self.__window._fg_color)
        self.__action_separator = ttk.Separator(self.__action_frame, orient="horizontal")
        self.__apply_button = ctk.CTkButton(self.__action_frame, text="Apply", font=("Arial", 20), height=2,
                                            command=self.__apply)
        self.__clear_button = ctk.CTkButton(self.__action_frame, text="Clear", font=("Arial", 20), height=2,
                                            command=self.__clear)
        self.__back_button = ctk.CTkButton(self.__action_frame, text="Back", font=("Arial", 20), height=2,
                                           command=self.__back)

        self.__menu_frame = ctk.CTkFrame(self.__window, fg_color=self.__window._fg_color)
        self.__menu_prompt = ctk.CTkLabel(self.__menu_frame, text="Select Filters", font=("Arial", 24))

        self.__typology_frame = ctk.CTkScrollableFrame(self.__window, fg_color=self.__window._fg_color)
        self.__type_vars_array = []
        self.__type_check_array = []
        for typology in self.__available_activities_list:
            var = ctk.BooleanVar(value=False)
            self.__type_vars_array.append(var)
            check = ctk.CTkCheckBox(self.__typology_frame, text=typology, font=("Arial", 18), onvalue=True,
                                    offvalue=False, hover=True, variable=var, command=self.__modify_view)
            self.__type_check_array.append(check)
        self.__subtype_frame = ctk.CTkFrame(self.__typology_frame, fg_color=self.__window._fg_color)
        self.__subtype_vars_array = []
        self.__subtype_check_array = []

        self.__filter_frame = ctk.CTkScrollableFrame(self.__window, fg_color=self.__window._fg_color)
        self.__typology_separator = ttk.Separator(self.__filter_frame, orient="vertical")
        self.__filter_separator = ttk.Separator(self.__filter_frame, orient="horizontal")
        self.__filter_frame.columnconfigure(index=0, weight=1)
        self.__filter_frame.columnconfigure(index=1, weight=1)

        self.__min_date_frame = ctk.CTkFrame(self.__filter_frame, fg_color=self.__window._fg_color)
        self.__min_data_label = ctk.CTkLabel(self.__min_date_frame, text="Initial Date", font=("Arial", 18))
        self.__min_data_dd = ctk.CTkEntry(self.__min_date_frame, width=40, placeholder_text="dd")
        self.__min_data_slash1 = ctk.CTkLabel(self.__min_date_frame, text="/", font=("Arial", 18))
        self.__min_data_mm = ctk.CTkEntry(self.__min_date_frame, width=40, placeholder_text="mm")
        self.__min_data_slash2 = ctk.CTkLabel(self.__min_date_frame, text="/", font=("Arial", 18))
        self.__min_data_yy = ctk.CTkEntry(self.__min_date_frame, width=60, placeholder_text="yyyy")

        self.__max_date_frame = ctk.CTkFrame(self.__filter_frame, fg_color=self.__window._fg_color)
        self.__max_data_label = ctk.CTkLabel(self.__max_date_frame, text="Final Date", font=("Arial", 18))
        self.__max_data_dd = ctk.CTkEntry(self.__max_date_frame, width=40, placeholder_text="dd")
        self.__max_data_slash1 = ctk.CTkLabel(self.__max_date_frame, text="/", font=("Arial", 18))
        self.__max_data_mm = ctk.CTkEntry(self.__max_date_frame, width=40, placeholder_text="mm")
        self.__max_data_slash2 = ctk.CTkLabel(self.__max_date_frame, text="/", font=("Arial", 18))
        self.__max_data_yy = ctk.CTkEntry(self.__max_date_frame, width=60, placeholder_text="yyyy")

        self.__min_duration_frame = ctk.CTkFrame(self.__filter_frame, fg_color=self.__window._fg_color)
        self.__min_duration_label = ctk.CTkLabel(self.__min_duration_frame, text="Min Duration", font=("Arial", 18))
        self.__min_duration_hh = ctk.CTkEntry(self.__min_duration_frame, width=40, placeholder_text="hh")
        self.__min_duration_slash1 = ctk.CTkLabel(self.__min_duration_frame, text=":", font=("Arial", 18))
        self.__min_duration_mm = ctk.CTkEntry(self.__min_duration_frame, width=40, placeholder_text="mm")
        self.__min_duration_slash2 = ctk.CTkLabel(self.__min_duration_frame, text=":", font=("Arial", 18))
        self.__min_duration_ss = ctk.CTkEntry(self.__min_duration_frame, width=40, placeholder_text="ss")

        self.__max_duration_frame = ctk.CTkFrame(self.__filter_frame, fg_color=self.__window._fg_color)
        self.__max_duration_label = ctk.CTkLabel(self.__max_duration_frame, text="Max Duration", font=("Arial", 18))
        self.__max_duration_hh = ctk.CTkEntry(self.__max_duration_frame, width=40, placeholder_text="hh")
        self.__max_duration_slash1 = ctk.CTkLabel(self.__max_duration_frame, text=":", font=("Arial", 18))
        self.__max_duration_mm = ctk.CTkEntry(self.__max_duration_frame, width=40, placeholder_text="mm")
        self.__max_duration_slash2 = ctk.CTkLabel(self.__max_duration_frame, text=":", font=("Arial", 18))
        self.__max_duration_ss = ctk.CTkEntry(self.__max_duration_frame, width=40, placeholder_text="ss")

        self.__fatigue_frame = ctk.CTkFrame(self.__filter_frame, fg_color=self.__window._fg_color)
        self.__fatigue_label = ctk.CTkLabel(self.__fatigue_frame, text="Fatigue (0-10)", font=("Arial", 18))
        self.__min_fatigue_label = ctk.CTkLabel(self.__fatigue_frame, text="Min:  ", font=("Arial", 16))
        self.__max_fatigue_label = ctk.CTkLabel(self.__fatigue_frame, text="Max:  ", font=("Arial", 16))
        self.__min_fatigue_entry = ctk.CTkEntry(self.__fatigue_frame, width=40, placeholder_text="0.0")
        self.__max_fatigue_entry = ctk.CTkEntry(self.__fatigue_frame, width=40, placeholder_text="10.0")

        self.__distance_frame = ctk.CTkFrame(self.__filter_frame, fg_color=self.__window._fg_color)
        self.__distance_label = ctk.CTkLabel(self.__distance_frame, text="Distance [km]", font=("Arial", 18))
        self.__min_distance_label = ctk.CTkLabel(self.__distance_frame, text="Min:", font=("Arial", 16))
        self.__max_distance_label = ctk.CTkLabel(self.__distance_frame, text="Max:", font=("Arial", 16))
        self.__min_distance_entry = ctk.CTkEntry(self.__distance_frame, width=40, placeholder_text="0.0")
        self.__max_distance_entry = ctk.CTkEntry(self.__distance_frame, width=40)

        self.__max_altitude_frame = ctk.CTkFrame(self.__filter_frame, fg_color=self.__window._fg_color)
        self.__max_altitude_label = ctk.CTkLabel(self.__max_altitude_frame, text="Maximum Altitude Reached [m]",
                                                 font=("Arial", 18))
        self.__min_max_altitude_label = ctk.CTkLabel(self.__max_altitude_frame, text="Min:", font=("Arial", 16))
        self.__max_max_altitude_label = ctk.CTkLabel(self.__max_altitude_frame, text="Max:", font=("Arial", 16))
        self.__min_max_altitude_entry = ctk.CTkEntry(self.__max_altitude_frame, width=50, placeholder_text="0.0")
        self.__max_max_altitude_entry = ctk.CTkEntry(self.__max_altitude_frame, width=50)

        self.__gain_frame = ctk.CTkFrame(self.__filter_frame, fg_color=self.__window._fg_color)
        self.__altitude_gain_label = ctk.CTkLabel(self.__gain_frame, text="Altitude Gained [m]", font=("Arial", 18))
        self.__min_altitude_gain_label = ctk.CTkLabel(self.__gain_frame, text="Min:", font=("Arial", 16))
        self.__max_altitude_gain_label = ctk.CTkLabel(self.__gain_frame, text="Max:", font=("Arial", 16))
        self.__min_altitude_gain_entry = ctk.CTkEntry(self.__gain_frame, width=40, placeholder_text="0.0")
        self.__max_altitude_gain_entry = ctk.CTkEntry(self.__gain_frame, width=40)

        self.__hot_frame = ctk.CTkFrame(self.__filter_frame, fg_color=self.__window._fg_color)
        self.__hot_label = ctk.CTkLabel(self.__hot_frame, text="Temperature Perceived", font=("Arial", 18))
        self.__hot_only_var = ctk.BooleanVar(value=False)
        self.__hot_only_check = ctk.CTkCheckBox(self.__hot_frame, text="Hot Only", font=("Arial", 16),
                                                onvalue=True, offvalue=False, hover=True, variable=self.__hot_only_var)
        self.__normal_only_var = ctk.BooleanVar(value=False)
        self.__normal_only_check = ctk.CTkCheckBox(self.__hot_frame, text="Normal Only", font=("Arial", 16),
                                                   onvalue=True, offvalue=False, hover=True,
                                                   variable=self.__normal_only_var)

        self.__rain_frame = ctk.CTkFrame(self.__filter_frame, fg_color=self.__window._fg_color)
        self.__rain_label = ctk.CTkLabel(self.__rain_frame, text="Weather", font=("Arial", 18))
        self.__rain_only_var = ctk.BooleanVar(value=False)
        self.__rain_only_check = ctk.CTkCheckBox(self.__rain_frame, text="Rain Only", font=("Arial", 16),
                                                 onvalue=True, offvalue=False, hover=True,
                                                 variable=self.__rain_only_var)
        self.__cloudy_only_var = ctk.BooleanVar(value=False)
        self.__cloudy_only_check = ctk.CTkCheckBox(self.__rain_frame, text="Normal Only", font=("Arial", 16),
                                                   onvalue=True, offvalue=False, hover=True,
                                                   variable=self.__cloudy_only_var)

        self.__indoor_frame = ctk.CTkFrame(self.__filter_frame, fg_color=self.__window._fg_color)
        self.__indoor_label = ctk.CTkLabel(self.__indoor_frame, text="Indoor", font=("Arial", 18))
        self.__indoor_only_var = ctk.BooleanVar(value=False)
        self.__indoor_only_check = ctk.CTkCheckBox(self.__indoor_frame, text="Indoor Only", font=("Arial", 16),
                                                   onvalue=True, offvalue=False, hover=True,
                                                   variable=self.__indoor_only_var)
        self.__outdoor_only_var = ctk.BooleanVar(value=False)
        self.__outdoor_only_check = ctk.CTkCheckBox(self.__indoor_frame, text="Outdoor Only", font=("Arial", 16),
                                                    onvalue=True, offvalue=False, hover=True,
                                                    variable=self.__outdoor_only_var)

        self.__pace_frame = ctk.CTkFrame(self.__filter_frame, fg_color=self.__window._fg_color)
        self.__pace_label = ctk.CTkLabel(self.__pace_frame, text="Pace [km/h]", font=("Arial", 18))
        self.__min_pace_label = ctk.CTkLabel(self.__pace_frame, text="Min:", font=("Arial", 16))
        self.__max_pace_label = ctk.CTkLabel(self.__pace_frame, text="Max:", font=("Arial", 16))
        self.__min_pace_entry = ctk.CTkEntry(self.__pace_frame, width=50, placeholder_text="mm")
        self.__max_pace_entry = ctk.CTkEntry(self.__pace_frame, width=50, placeholder_text="mm")

        self.__generic_frames = [self.__max_date_frame, self.__min_date_frame, self.__max_duration_frame,
                                 self.__min_duration_frame, self.__fatigue_frame]
        self.__non_generic_frames = [self.__distance_frame, self.__max_altitude_frame, self.__pace_frame,
                                     self.__gain_frame, self.__hot_frame, self.__rain_frame, self.__indoor_frame]

        self.__non_generic_entries = [self.__min_distance_entry, self.__max_distance_entry,
                                      self.__min_max_altitude_entry,
                                      self.__max_max_altitude_entry, self.__min_altitude_gain_entry,
                                      self.__max_altitude_gain_entry, self.__min_pace_entry, self.__max_pace_entry]

        self.__non_generic_checks = [self.__hot_only_check, self.__normal_only_check,
                                     self.__rain_only_check, self.__cloudy_only_check, self.__indoor_only_check,
                                     self.__outdoor_only_check]

        self.__generic_entries = [self.__min_data_dd, self.__min_data_mm, self.__min_data_yy,
                                  self.__max_data_dd, self.__max_data_mm, self.__max_data_yy,
                                  self.__min_duration_hh, self.__min_duration_mm, self.__min_duration_ss,
                                  self.__max_duration_hh, self.__max_duration_mm, self.__max_duration_ss,
                                  self.__min_fatigue_entry, self.__max_fatigue_entry]

    def run(self):
        self.__assembly()
        self.__window.mainloop()
        return self.__out

    def __assembly(self):
        self.__menu_frame.pack(side="top", expand=False, fill="x")
        self.__menu_prompt.pack(side="top", expand=False, anchor="w", padx=20, pady=20)

        self.__bottom_frame.pack(side="bottom", expand=False, fill="x")
        self.__bottom_separator.pack(side="top", expand=False, fill="x")
        self.__back_to_menu_button.pack(side="left", expand=False, padx=20, pady=20)
        self.__exit_button.pack(side="right", expand=False, padx=20, pady=20)

        self.__action_frame.pack(side="bottom", expand=False, fill="x")
        self.__action_separator.pack(side="top", expand=False, fill="x")
        self.__apply_button.pack(side="left", expand=False, padx=20, pady=20)
        self.__clear_button.pack(side="right", expand=False, padx=20, pady=20)
        self.__back_button.pack(side="right", expand=True, padx=20, pady=20)

        self.__typology_frame.pack(side="left", expand=False, fill="both")

        self.__filter_frame.pack(side="right", expand=True, fill="both")
        # self.__typology_separator.pack(side="left", expand=False, fill="y")

        for i, check in enumerate(self.__type_check_array):
            check.grid(row=2*i, column=0, sticky="w", padx=20, pady=20)

        self.__min_data_label.pack(side="top", anchor="w", expand=False, padx=20, pady=5)
        self.__min_data_dd.pack(side="left", anchor="w", expand=False, padx=(20, 2), pady=5)
        self.__min_data_slash1.pack(side="left", anchor="w", expand=False, padx=2, pady=5)
        self.__min_data_mm.pack(side="left", anchor="w", expand=False, padx=2, pady=5)
        self.__min_data_slash2.pack(side="left", anchor="w", expand=False, padx=2, pady=5)
        self.__min_data_yy.pack(side="left", anchor="w", expand=False, padx=(2, 20), pady=5)

        self.__max_data_label.pack(side="top", anchor="w", expand=False, padx=20, pady=5)
        self.__max_data_dd.pack(side="left", anchor="w", expand=False, padx=(20, 2), pady=5)
        self.__max_data_slash1.pack(side="left", anchor="w", expand=False, padx=2, pady=5)
        self.__max_data_mm.pack(side="left", anchor="w", expand=False, padx=2, pady=5)
        self.__max_data_slash2.pack(side="left", anchor="w", expand=False, padx=2, pady=5)
        self.__max_data_yy.pack(side="left", anchor="w", expand=False, padx=(2, 20), pady=5)

        self.__min_duration_label.pack(side="top", anchor="w", expand=False, padx=20, pady=5)
        self.__min_duration_hh.pack(side="left", anchor="w", expand=False, padx=(20, 2), pady=5)
        self.__min_duration_slash1.pack(side="left", anchor="w", expand=False, padx=2, pady=5)
        self.__min_duration_mm.pack(side="left", anchor="w", expand=False, padx=2, pady=5)
        self.__min_duration_slash2.pack(side="left", anchor="w", expand=False, padx=2, pady=5)
        self.__min_duration_ss.pack(side="left", anchor="w", expand=False, padx=(2, 20), pady=5)

        self.__max_duration_label.pack(side="top", anchor="w", expand=False, padx=20, pady=5)
        self.__max_duration_hh.pack(side="left", anchor="w", expand=False, padx=(20, 2), pady=5)
        self.__max_duration_slash1.pack(side="left", anchor="w", expand=False, padx=2, pady=5)
        self.__max_duration_mm.pack(side="left", anchor="w", expand=False, padx=2, pady=5)
        self.__max_duration_slash2.pack(side="left", anchor="w", expand=False, padx=2, pady=5)
        self.__max_duration_ss.pack(side="left", anchor="w", expand=False, padx=(2, 20), pady=5)

        self.__fatigue_label.pack(side="top", anchor="w", expand=False, padx=20, pady=5)
        self.__min_fatigue_label.pack(side="left", anchor="w", expand=False, padx=(20, 2), pady=5)
        self.__min_fatigue_entry.pack(side="left", anchor="w", expand=False, padx=0, pady=5)
        self.__max_fatigue_label.pack(side="left", anchor="w", expand=False, padx=(20, 2), pady=5)
        self.__max_fatigue_entry.pack(side="left", anchor="w", expand=False, padx=0, pady=5)

        self.__distance_label.pack(side="top", anchor="w", expand=False, padx=20, pady=5)
        self.__min_distance_label.pack(side="left", anchor="w", expand=False, padx=(20, 2), pady=5)
        self.__min_distance_entry.pack(side="left", anchor="w", expand=False, padx=2, pady=5)
        self.__max_distance_label.pack(side="left", anchor="w", expand=False, padx=(20, 2), pady=5)
        self.__max_distance_entry.pack(side="left", anchor="w", expand=False, padx=2, pady=5)

        self.__pace_label.pack(side="top", anchor="w", expand=False, padx=20, pady=5)
        self.__min_pace_label.pack(side="left", anchor="w", expand=False, padx=(20, 2), pady=5)
        self.__min_pace_entry.pack(side="left", anchor="w", expand=False, padx=2, pady=5)
        self.__max_pace_label.pack(side="left", anchor="w", expand=False, padx=(20, 2), pady=5)
        self.__max_pace_entry.pack(side="left", anchor="w", expand=False, padx=2, pady=5)

        self.__max_altitude_label.pack(side="top", anchor="w", expand=False, padx=20, pady=5)
        self.__min_max_altitude_label.pack(side="left", anchor="w", expand=False, padx=(20, 2), pady=5)
        self.__min_max_altitude_entry.pack(side="left", anchor="w", expand=False, padx=2, pady=5)
        self.__max_max_altitude_label.pack(side="left", anchor="w", expand=False, padx=(20, 2), pady=5)
        self.__max_max_altitude_entry.pack(side="left", anchor="w", expand=False, padx=2, pady=5)

        self.__altitude_gain_label.pack(side="top", anchor="w", expand=False, padx=20, pady=5)
        self.__min_altitude_gain_label.pack(side="left", anchor="w", expand=False, padx=(20, 2), pady=5)
        self.__min_altitude_gain_entry.pack(side="left", anchor="w", expand=False, padx=2, pady=5)
        self.__max_altitude_gain_label.pack(side="left", anchor="w", expand=False, padx=(20, 2), pady=5)
        self.__max_altitude_gain_entry.pack(side="left", anchor="w", expand=False, padx=2, pady=5)

        self.__hot_label.pack(side="top", anchor="w", expand=False, padx=20, pady=5)
        self.__hot_only_check.pack(side="left", anchor="w", expand=False, padx=(20, 2), pady=5)
        self.__normal_only_check.pack(side="left", anchor="w", expand=False, padx=(20, 2), pady=5)

        self.__rain_label.pack(side="top", anchor="w", expand=False, padx=20, pady=5)
        self.__rain_only_check.pack(side="left", anchor="w", expand=False, padx=(20, 2), pady=5)
        self.__cloudy_only_check.pack(side="left", anchor="w", expand=False, padx=(20, 2), pady=5)

        self.__indoor_label.pack(side="top", anchor="w", expand=False, padx=20, pady=5)
        self.__indoor_only_check.pack(side="left", anchor="w", expand=False, padx=(20, 2), pady=5)
        self.__outdoor_only_check.pack(side="left", anchor="w", expand=False, padx=(20, 2), pady=5)

        self.__min_date_frame.grid(row=0, column=0, padx=20, pady=10, sticky="w")
        self.__max_date_frame.grid(row=0, column=1, padx=20, pady=10, sticky="w")
        self.__min_duration_frame.grid(row=1, column=0, padx=20, pady=10, sticky="w")
        self.__max_duration_frame.grid(row=1, column=1, padx=20, pady=10, sticky="w")
        self.__fatigue_frame.grid(row=2, column=0, padx=20, pady=10, columnspan=2, sticky="w")

    def __close(self):
        ans = tk.messagebox.askyesno(title="Exit", message="Are you sure you want to quit?")  # create messagebox
        if ans:  # if yes, close
            self.__window.destroy()  # close the home window
            self.__out = {"command": "close", "argument": None}
        else:  # if no, ignore
            pass
        # self.window.destroy()

    def __back_to_menu(self):
        self.__window.destroy()
        self.__out = {"command": "home", "argument": None}

    def __back(self):
        ans = messagebox.askokcancel(title="Back", message="Are you sure? You will lose all your progress")
        if ans:
            self.__window.destroy()
            self.__out = {"command": "navigate", "argument": self.__history.get_list()}
        else:
            pass

    def __clear(self):
        for i in range(len(self.__available_activities_list)):
            self.__type_check_array[i].deselect()

        for feature in self.__non_generic_checks:
            feature.deselect()

        for feature in self.__non_generic_entries:
            feature.delete(0, tk.END)

        for feature in self.__non_generic_frames:
            feature.place_forget()

        for feature in self.__generic_entries:
            feature.delete(0, tk.END)

    def __apply(self):
        mask = {"item_type": None, "min_date": None, "max_date": None, "min_distance": None, "max_distance": None,
                "min_duration": None, "max_duration": None, "min_pace": None, "max_pace": None,
                "min_fatigue": None, "max_fatigue": None, "min_max_altitude": None, "max_max_altitude": None,
                "min_altitude_gain": None, "max_altitude_gain": None, "equipment": None, "typology": None,
                "indoor": None, "hot": None, "rain": None}

        # ---- filter by type ------------------------------------------------------------------------------------------
        types = []
        for i in range(len(self.__available_activities_list)):
            if self.__type_vars_array[i].get():
                types.append(self.__available_activities_list[i])
        if len(types) > 0:
            mask["item_type"] = types

        # ---- filter by type ------------------------------------------------------------------------------------------
        subtypes = []
        if len(types) == 1:
            for i in range(len(self.__subtype_check_array)):
                if self.__subtype_vars_array[i].get():
                    subtypes.append(self.__subtype_check_array[i].cget("text"))
            print(subtypes)
            if len(subtypes) > 0:
                mask["typology"] = subtypes

        # ---- filter by data ------------------------------------------------------------------------------------------
        try:
            min_dd = int(self.__min_data_dd.get())
            min_mm = int(self.__min_data_mm.get())
            min_yy = int(self.__min_data_yy.get())
            self.__min_date = datetime.date(min_yy, min_mm, min_dd)
        except ValueError:
            pass
        try:
            max_dd = int(self.__max_data_dd.get())
            max_mm = int(self.__max_data_mm.get())
            max_yy = int(self.__max_data_yy.get())
            self.__max_date = datetime.date(max_yy, max_mm, max_dd)
        except ValueError:
            pass
        if not (self.__min_date is None and self.__max_date is None):
            if self.__min_date is None:
                self.__min_date = datetime.date(2000, 1, 1)
            if self.__max_date is None:
                self.__max_date = datetime.date.today() + datetime.timedelta(days=1)
            if self.__min_date < self.__max_date:
                mask["min_date"] = self.__min_date
                mask["max_date"] = self.__max_date

        # ---- filter by duration --------------------------------------------------------------------------------------
        try:
            min_hh = int(self.__min_duration_hh.get())
            min_mm = int(self.__min_duration_mm.get())
            min_ss = int(self.__min_duration_ss.get())
            self.__min_duration = Duration(hours=min_hh, minutes=min_mm, seconds=min_ss)
        except ValueError:
            pass
        try:
            max_hh = int(self.__max_duration_hh.get())
            max_mm = int(self.__max_duration_mm.get())
            max_ss = int(self.__max_duration_ss.get())
            self.__max_duration = Duration(hours=max_hh, minutes=max_mm, seconds=max_ss)
        except ValueError:
            pass
        if not (self.__min_duration is None and self.__max_duration is None):
            if self.__min_duration is None:
                self.__min_duration = Duration(total_seconds=0)
            if self.__max_duration is None:
                self.__max_duration = Duration(hours=23, minutes=59, seconds=59)
            if self.__min_duration < self.__max_duration:
                mask["min_duration"] = self.__min_duration
                mask["max_duration"] = self.__max_duration

        # ---- filter by fatigue ---------------------------------------------------------------------------------------
        try:
            self.__min_fatigue = float(self.__min_fatigue_entry.get())
        except ValueError:
            pass
        try:
            self.__max_fatigue = float(self.__max_fatigue_entry.get())
        except ValueError:
            pass
        if not (self.__min_fatigue is None and self.__max_fatigue is None):
            if self.__min_fatigue is None or self.__min_fatigue < 0:
                self.__min_fatigue = 0
            if self.__max_date is None or self.__max_fatigue > 10:
                self.__max_fatigue = 10
            if self.__min_fatigue < self.__max_fatigue:
                mask["min_fatigue"] = self.__min_fatigue
                mask["max_fatigue"] = self.__max_fatigue

        # ---- filter by distance --------------------------------------------------------------------------------------
        try:
            self.__min_distance = float(self.__min_distance_entry.get())
        except ValueError:
            pass
        try:
            self.__max_distance = float(self.__max_distance_entry.get())
        except ValueError:
            pass
        if not (self.__min_distance is None and self.__max_distance is None):
            if self.__min_distance is None or self.__min_distance < 0:
                self.__min_distance = 0
            if self.__max_distance is None:
                self.__max_distance = 1000
            if self.__min_distance < self.__max_distance:
                mask["min_distance"] = self.__min_distance
                mask["max_distance"] = self.__max_distance

        # ---- filter by max altitude reached --------------------------------------------------------------------------
        try:
            self.__min_max_altitude = float(self.__min_max_altitude_entry.get())
        except ValueError:
            pass
        try:
            self.__max_max_altitude = float(self.__max_max_altitude_entry.get())
        except ValueError:
            pass
        if not (self.__min_max_altitude is None and self.__max_max_altitude is None):
            if self.__min_max_altitude is None or self.__min_max_altitude < 0:
                self.__min_max_altitude = 0
            if self.__max_max_altitude is None:
                self.__max_max_altitude = 8848
            if self.__min_max_altitude < self.__max_max_altitude:
                mask["min_max_altitude"] = self.__min_max_altitude
                mask["max_max_altitude"] = self.__max_max_altitude

        # ---- filter by altitude gained -------------------------------------------------------------------------------
        try:
            self.__min_altitude_gain = float(self.__min_altitude_gain_entry.get())
        except ValueError:
            pass
        try:
            self.__max_altitude_gain = float(self.__max_altitude_gain_entry.get())
        except ValueError:
            pass
        if not (self.__min_altitude_gain is None and self.__max_altitude_gain is None):
            if self.__min_altitude_gain is None or self.__min_altitude_gain < 0:
                self.__min_altitude_gain = 0
            if self.__max_altitude_gain is None:
                self.__max_altitude_gain = 20000
            if self.__min_altitude_gain < self.__max_altitude_gain:
                mask["min_altitude_gain"] = self.__min_altitude_gain
                mask["max_altitude_gain"] = self.__max_altitude_gain
        # ---- filter by temperature -----------------------------------------------------------------------------------
        try:
            hot = self.__hot_only_var.get()
            normal = self.__normal_only_var.get()
        except ValueError:
            print("Error in HOT")
            pass
        else:
            if hot and not normal:
                self.__hot = True
            elif normal and not hot:
                self.__hot = False
            else:
                self.__hot = None
            mask["hot"] = self.__hot

        # ---- filter by weather ---------------------------------------------------------------------------------------
        try:
            rain = self.__rain_only_var.get()
            normal = self.__cloudy_only_var.get()
        except ValueError:
            print("Error in RAIN")
            pass
        else:
            if rain and not normal:
                self.__rain = True
            elif normal and not rain:
                self.__rain = False
            else:
                self.__rain = None
            mask["rain"] = self.__rain

        # ---- filter by indoor -----------------------------------------------------------------------------------
        try:
            indoor = self.__indoor_only_var.get()
            outdoor = self.__outdoor_only_var.get()
        except ValueError:
            print("Error in HOT")
            pass
        else:
            if indoor and not outdoor:
                self.__indoor = True
            elif outdoor and not indoor:
                self.__indoor = False
            else:
                self.__indoor = None
            mask["indoor"] = self.__indoor

        # ---- filter by pace ------------------------------------------------------------------------------------------
        try:
            self.__min_pace = float(self.__min_pace_entry.get())
        except ValueError:
            pass
        try:
            self.__max_pace = float(self.__max_pace_entry.get())
        except ValueError:
            pass
        if not (self.__min_pace is None and self.__max_pace is None):
            if self.__min_pace is None:
                self.__min_pace = 0
            if self.__max_pace is None:
                self.__max_pace = 100
            if self.__min_pace < self.__max_pace:
                mask["min_pace"] = self.__min_pace
                mask["max_pace"] = self.__max_pace

        # ---- out -----------------------------------------------------------------------------------------------------
        print(mask)
        self.__window.destroy()
        self.__out = {"command": "navigate_activities", "argument": mask}

    def __modify_view(self):
        state = []
        for i in range(len(self.__type_vars_array)):
            if self.__type_vars_array[i].get():
                state.append(self.__available_activities_list[i])
        if len(state) != 1:
            self.__subtype_frame.grid_forget()
            self.__generic_assembly()
        else:
            if state[0] in ["Run", "Hike", "Bike"]:
                self.__distance_activity_assembly(state[0])
            elif state[0] in ["Sport", "Gym"]:
                self.__generic_activity_assembly(state[0])
            else:
                sys.exit("Invalid activity")

    def __generic_assembly(self):
        for feature in self.__non_generic_frames:
            feature.grid_forget()
        for feature in self.__non_generic_entries:
            feature.delete(0, tk.END)
        for feature in self.__non_generic_checks:
            feature.deselect()

    def __distance_activity_assembly(self, selected):
        for feature in self.__non_generic_frames:
            feature.grid_forget()
        for feature in self.__non_generic_entries:
            feature.delete(0, tk.END)
        for feature in self.__non_generic_checks:
            feature.deselect()

        self.__display_subtype(selected)
        self.__distance_frame.grid(row=3, column=0, padx=20, pady=10, sticky="w")
        self.__pace_frame.grid(row=3, column=1, padx=20, pady=10, sticky="w")
        self.__max_altitude_frame.grid(row=4, column=0, padx=20, pady=10, sticky="w")
        self.__gain_frame.grid(row=4, column=1, padx=20, pady=10, sticky="w")
        self.__hot_frame.grid(row=5, column=0, padx=20, pady=10, sticky="w")
        self.__rain_frame.grid(row=5, column=1, padx=20, pady=10, sticky="w")

    def __generic_activity_assembly(self, selected):
        for feature in self.__non_generic_frames:
            feature.grid_forget()
        for feature in self.__non_generic_entries:
            feature.delete(0, tk.END)
        for feature in self.__non_generic_checks:
            feature.deselect()

        self.__display_subtype(selected)
        self.__hot_frame.grid(row=5, column=0, padx=20, pady=10, sticky="w")
        self.__indoor_frame.grid(row=5, column=1, padx=20, pady=10, sticky="w")

    def __display_subtype(self, selected):
        for check in self.__subtype_check_array:
            check.deselect()
            check.pack_forget()
        self.__subtype_vars_array = []
        self.__subtype_check_array = []

        if selected in ["Run", "Hike", "Bike"]:
            typologies = DistanceActivity.get_available(selected)
        elif selected in ["Gym", "Sport"]:
            typologies = GeneralActivity.get_available(selected)
        else:
            raise ValueError

        for t in typologies:
            var = tk.BooleanVar(value=False)
            check = ctk.CTkCheckBox(self.__subtype_frame, text=t, font=("Arial", 20), onvalue=True, offvalue=False,
                                    hover=True, variable=var)
            self.__subtype_vars_array.append(var)
            self.__subtype_check_array.append(check)
            check.pack(side="top", anchor="w", padx=(40, 0), pady=5)

        index = self.__available_activities_list.index(selected)
        self.__subtype_frame.grid(row=2*index+1, sticky="w", padx=20, pady=10)


########################################################################################################################
########################################################################################################################
########################################################################################################################


def filter_equipment(items_list, mask):
    if mask["typology"] is not None:
        items_list = filter(lambda x: x.get_typology() in mask["typology"], items_list)

    if mask["alive"] is not None:
        items_list = filter(lambda x: x.get_alive() == mask["alive"], items_list)

    if mask["consumable"] is not None:
        items_list = filter(lambda x: x.get_typology() in Equipment.get_consumable(), items_list)
    return list(items_list)


class FilterGarage:
    def __init__(self, garage: Garage):
        self.__garage = garage
        self.__out = {"command": "close", "argument": None}
        self.__typologies = None
        self.__alive = None
        self.__available_typologies_list = [None, "Running Shoes", "Walking Shoes", "Hiking Boots", "Mountain Bike",
                                            "Road Bike", "Skis", "Football Shoes"]

        ctk.set_appearance_mode("system")
        ctk.set_default_color_theme("green")
        self.__window = ctk.CTk()  # create window
        self.__window.geometry("600x600+10+10")
        self.__window.resizable(False, False)
        self.__window.title("Sport Tracker: Navigate Garage Menu")
        # call the specified function, if the user clicks the red arrow
        self.__window.protocol("WM_DELETE_WINDOW", self.__close)

        self.__bottom_frame = ctk.CTkFrame(self.__window)
        self.__bottom_separator = ttk.Separator(self.__bottom_frame, orient="horizontal")
        self.__back_to_menu_button = ctk.CTkButton(self.__bottom_frame, text="Menu", font=("Arial", 20), height=2,
                                                   command=self.__back_to_menu)
        self.__exit_button = ctk.CTkButton(self.__bottom_frame, text="Exit", font=("Arial", 20), height=2,
                                           command=self.__close)

        self.__action_frame = ctk.CTkFrame(self.__window)
        self.__action_separator = ttk.Separator(self.__action_frame, orient="horizontal")
        self.__apply_button = ctk.CTkButton(self.__action_frame, text="Apply", font=("Arial", 20), height=2,
                                            command=self.__apply)
        self.__clear_button = ctk.CTkButton(self.__action_frame, text="Clear", font=("Arial", 20), height=2,
                                            command=self.__clear)

        self.__menu_frame = ctk.CTkFrame(self.__window)
        self.__menu_separator = ttk.Separator(self.__menu_frame, orient="horizontal")
        self.__menu_prompt = ctk.CTkLabel(self.__menu_frame, text="Select Filters", font=("Arial", 24))

        self.__typology_frame = ctk.CTkFrame(self.__window)
        self.__typology_separator = ttk.Separator(self.__typology_frame, orient="vertical")
        self.__type_vars_array = []
        self.__type_check_array = []
        for typology in self.__available_typologies_list[1:]:
            var = ctk.BooleanVar(value=True)
            self.__type_vars_array.append(var)
            check = ctk.CTkCheckBox(self.__typology_frame, text=typology, font=("Arial", 18), onvalue=True,
                                    offvalue=False, hover=True, variable=var)
            self.__type_check_array.append(check)

        self.__filter_frame = ctk.CTkFrame(self.__window)
        self.__alive_var = ctk.BooleanVar(value=True)
        self.__alive_check = ctk.CTkCheckBox(self.__filter_frame, text="Usable Only", font=("Arial", 18), onvalue=True,
                                             offvalue=False, hover=True, variable=self.__alive_var)

        self.__dead_var = ctk.BooleanVar(value=False)
        self.__dead_check = ctk.CTkCheckBox(self.__filter_frame, text="Not Usable Only", font=("Arial", 18),
                                            onvalue=True,
                                            offvalue=False, hover=True, variable=self.__dead_var)

        self.__consumable_var = ctk.BooleanVar(value=False)
        self.__consumable_check = ctk.CTkCheckBox(self.__filter_frame, text="Consumable Only", font=("Arial", 18),
                                                  onvalue=True,
                                                  offvalue=False, hover=True, variable=self.__consumable_var)

    def run(self):
        self.__assembly()
        self.__window.mainloop()
        return self.__out

    def __assembly(self):
        self.__menu_frame.pack(side="top", expand=False, fill="x")
        self.__menu_prompt.pack(expand=False, anchor="w", padx=20, pady=20)

        self.__bottom_frame.pack(side="bottom", expand=False, fill="x")
        self.__bottom_separator.pack(side="top", expand=False, fill="x")
        self.__back_to_menu_button.pack(side="left", expand=False, anchor="w", padx=20, pady=20)
        self.__exit_button.pack(side="right", expand=False, anchor="w", padx=20, pady=20)

        self.__action_frame.pack(side="bottom", expand=False, fill="x")
        self.__action_separator.pack(side="top", expand=False, fill="x")
        self.__apply_button.pack(side="left", expand=False, anchor="w", padx=20, pady=20)
        self.__clear_button.pack(side="right", expand=False, anchor="w", padx=20, pady=20)

        self.__typology_frame.pack(side="left", expand=False, fill="both")
        self.__typology_separator.pack(side="right", expand=False, fill="y")
        for i in range(len(self.__available_typologies_list) - 1):
            self.__type_check_array[i].pack(side="top", expand=False, anchor="w", padx=20, pady=10)

        self.__filter_frame.pack(side="right", expand=True, fill="both")
        self.__alive_check.pack(side="top", expand=False, anchor="w", padx=20, pady=10)
        self.__dead_check.pack(side="top", expand=False, anchor="w", padx=20, pady=10)
        self.__consumable_check.pack(side="top", expand=False, anchor="w", padx=20, pady=10)

    def __close(self):
        ans = tk.messagebox.askyesno(title="Exit", message="Are you sure you want to quit?")  # create messagebox
        if ans:  # if yes, close
            self.__window.destroy()  # close the home window
            self.__out = {"command": "close", "argument": None}
        else:  # if no, ignore
            pass
        # self.window.destroy()

    def __back_to_menu(self):
        self.__window.destroy()
        self.__out = {"command": "continue", "argument": None}

    def __clear(self):
        for i in range(len(self.__available_typologies_list) - 1):
            self.__type_check_array[i].deselect()
        self.__alive_check.deselect()
        self.__dead_check.deselect()
        self.__consumable_check.deselect()

    def __apply(self):
        mask = {"typology": None, "alive": None, "consumable": None}
        types = []
        for i in range(len(self.__available_typologies_list) - 1):
            if self.__type_vars_array[i].get() == 1:
                types.append(self.__available_typologies_list[i + 1])
        if len(types) > 0:
            mask["typology"] = types

        if self.__alive_var.get() and not self.__dead_var.get():
            mask["alive"] = True
        if self.__dead_var.get() and not self.__alive_var.get():
            mask["alive"] = False

        if self.__consumable_var.get():
            mask["consumable"] = True

        self.__window.destroy()
        self.__out = {"command": "navigate", "argument": mask}
