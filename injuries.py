import pandas as pd
import datetime
import sys


class MedicalCentre:
    def __init__(self, files):
        self.__file = [files[0], "injuries"]  # filename and sheet_name
        self.__backup_file = [files[1], "injuries"]  # filename and sheet_name
        self.__backup(download=True)
        self.__injuries_list = self.__read()

    def __read(self):
        filename = self.get_file()
        items = []
        reader = pd.read_excel(filename[0], sheet_name=filename[1])
        for _, row in reader.iterrows():
            item = Injury(row)
            items.append(item)
        return items

    def write(self):
        filename = self.get_file()
        file = []
        for item in self.get_list():
            ans = item.write()
            file.append(ans)
        file = pd.DataFrame(file)
        with pd.ExcelWriter(filename[0], mode="a", if_sheet_exists="replace") as writer:
            file.to_excel(writer, sheet_name=filename[1], index=False)
        self.__backup(False)

    def __backup(self, download):
        """ download is True if we want to restore the data from the backup file to the actual database
        download is False if we are storing the updated data to the backup, after a successful writing operation"""
        if download:
            source = self.get_backup_file()
            target = self.get_file()
        else:
            source = self.get_file()
            target = self.get_backup_file()

        file = pd.read_excel(source[0], sheet_name=source[1])
        with pd.ExcelWriter(target[0], mode="a", if_sheet_exists="replace") as writer:
            file.to_excel(writer, sheet_name=target[1], index=False)

    def get_list(self):
        return self.__injuries_list

    def get_file(self):
        return self.__file

    def get_backup_file(self):
        return self.__backup_file

    def add_item(self, item):
        self.__injuries_list.append(item)

    def remove_item(self, item):
        for i, equipment in enumerate(self.__injuries_list):
            if equipment == item:
                self.__injuries_list.pop(i)
                break


class Injury:
    def __init__(self, sequence):
        self.__issue = None
        self.__date = None
        self.__fatal = None
        self.__duration = None
        self.__notes = None
        # if adding new available, remember to propagate in the static method and in the activities
        self.__read(sequence)

    def __read(self, sequence):
        self.__issue = sequence["issue"]
        dd, mm, yy = sequence["date_day":"date_year"]
        self.__date = datetime.date(year=yy, month=mm, day=dd)
        self.__fatal = True if sequence["fatal"] == "True" else False
        self.__duration = (sequence["duration"])
        self.__notes = sequence["notes"]

    def write(self):
        if isinstance(self.__date, datetime.date) and isinstance(self.__duration, int) and self.__fatal in [True, False]:
            ans = {"issue": self.__issue, "date_day": self.__date.day, "date_month": self.__date.month,
                   "date_year": self.__date.year, "duration": self.__duration, "fatal": self.__fatal, "notes": self.__notes}
        else:
            sys.exit("Error while writing")
        return ans

    def __str__(self):
        return f"{self.__issue}\n" \
               f"Date: {self.__date} -> Weeks of stop: {self.__duration if self.__fatal else None}\n" \
               f"Notes:{self.__notes}\n"

    def get_issue(self):
        return self.__issue

    def get_date(self):
        return self.__date

    def get_duration(self):
        return self.__duration

    def get_fatal(self):
        return self.__fatal

    def get_notes(self):
        return self.__notes

