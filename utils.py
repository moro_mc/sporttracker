import numpy as np


class Duration:
    """Store a duration, in terms of days - hours - minutes - seconds and total seconds.
    It can be initialised in either one or the other mode. Note that negative durations are not allowed, hence all
    arguments must be positive; there is no upper limit to the values.
    Contains the basic operators to sum, subtract and compare the values among two Duration objects.
    """
    def __init__(self, days: int = 0, hours: int = 0, minutes: int = 0, seconds: int = 0, total_seconds: int = 0):
        # check only one initialization modality: either through days-hours-minutes-seconds, or through total_seconds
        if any([days, minutes, hours, seconds]) != 0 and total_seconds != 0:
            raise ValueError("You cannot use total_seconds when using other arguments!")
        if any([x < 0 for x in [days, minutes, hours, seconds, total_seconds]]):  # positive arguments only are accepted
            raise ValueError("The arguments cannot be negative")
        # initialize the attributes using the values provided
        self.days = days
        self.hours = hours
        self.minutes = minutes
        self.seconds = seconds
        if total_seconds != 0:  # if using the second modality, initialize through that value
            self.__total = total_seconds
        else:  # if using the first modality, use the function to initialize the total_seconds attribute
            self.__total = self.__to_seconds()
        self.__to_days()  # compute the fields in canonical version (hours<24, minutes<60, seconds<60)

    def __str__(self):
        """ print a string, in days-hours-minutes-seconds format. if a field is null, it is not printed"""
        string = f""
        if self.days != 0:
            string += f"{self.days}d"
            if self.hours != 0 or self.minutes != 0 or self.seconds != 0:
                string += f" "
        if self.hours != 0:
            string += f"{self.hours}h"
            if self.minutes != 0 or self.seconds != 0:
                string += f" "
        if self.minutes != 0:
            string += f"{self.minutes}m"
            if self.seconds != 0:
                string += f" "
        if any(x != 0 for x in [self.days, self.hours, self.minutes]) and self.seconds == 0:
            pass
        else:
            string += f"{self.seconds}s"
        return string

    def __add__(self, other):
        # sum up the total seconds of both elements, initialize and return a new instance
        total = self.__total + other.get_total_seconds()
        return Duration(total_seconds=total)

    def __sub__(self, other):
        if self.__total < other.get_total_seconds():  # the duration cannot be negative; return zero duration
            total = Duration(total_seconds=0)
        else:
            total = self.__total - other.get_total_seconds()  # subtract the total seconds, initialize and return an instance
        return Duration(total_seconds=total)

    def __lt__(self, other):
        if self.__total < other.get_total_seconds():  # compare the total seconds, return the corresponding boolean
            return True
        else:
            return False

    def __gt__(self, other):
        if self.__total > other.get_total_seconds():  # compare the total seconds, return the corresponding boolean
            return True
        else:
            return False

    def __le__(self, other):
        if self.__total <= other.get_total_seconds():  # compare the total seconds, return the corresponding boolean
            return True
        else:
            return False

    def __ge__(self, other):
        if self.__total >= other.get_total_seconds():  # compare the total seconds, return the corresponding boolean
            return True
        else:
            return False

    def __eq__(self, other):
        if self.__total == other.get_total_seconds():  # compare the total seconds, return the corresponding boolean
            return True
        else:
            return False

    def __ne__(self, other):
        if self.__total != other.get_total_seconds():  # compare the total seconds, return the corresponding boolean
            return True
        else:
            return False

    def __isub__(self, other):
        if self.__total <= other.get_total_seconds():  # the duration cannot be negative; return zero duration
            self.__total = 0
        else:
            self.__total -= other.get_total_seconds()  # subtract the total seconds of the second instance to those of the first
        self.__to_days()  # convert in canonical form
        return self  # return the current object

    def __iadd__(self, other):
        self.__total += other.get_total_seconds()  # sum the total seconds of the second instance to the ones of the first
        self.__to_days()  # convert in canonical form
        return self  # return the current object

    def __to_seconds(self):
        """ convert the time into seconds"""
        time = 0  # initialize null
        # sum up all the attributes, after converting them into seconds
        time += self.days * 3600 * 24
        time += self.hours * 3600
        time += self.minutes * 60
        time += self.seconds
        return time

    def __to_days(self):
        """convert the data into days, hours, minutes and seconds,
        in canonical version: hours<24, minutes<60, seconds<60"""
        time = self.__total  # start from the time expressed in total seconds
        dd = int(np.floor(time / 24 / 3600))  # convert the time in days, extract the integer part; those are the days
        time -= dd * 24 * 3600  # compute the time left after subtracting the days (converted in seconds)
        # repeat for hours and minutes
        hh = int(np.floor(time / 3600))
        time -= hh * 3600
        mm = int(np.floor(time / 60))
        time -= mm * 60
        ss = time  # what remains are the seconds
        # store to the attributes
        self.days = dd
        self.hours = hh
        self.minutes = mm
        self.seconds = ss

    def get_total_seconds(self):
        """return the time as total seconds"""
        return self.__total

    def to_minutes(self):
        """return the time as total minutes"""
        return self.__total/60

    def to_hours(self):
        """return the time as total hours"""
        return self.__total/3600

    def to_days(self):
        """return the time as total days"""
        return self.__total/3600/24


def main():
    time = Duration(total_seconds=548659)
    time2 = Duration(1, 6, 8, 45)
    # time_no = Duration(-2)
    # print(time_no)
    print(time)
    print(time2)
    print(time + time2)
    print(time - time2)
    print(time < time2)
    print(time > time2)
    print(time == time2)
    print(time != time2)
    print(time == time)
    time += time2
    print(time)
    time -= time2
    print(time.to_days())
    print(time.to_hours())
    print(time.to_minutes())
    print(time.get_total_seconds())


if __name__ == '__main__':
    main()
