from activities import History, GeneralActivity, DistanceActivity
from equipment import Garage, Equipment
from injuries import MedicalCentre, Injury
from menu_gui import MenuGUI
from filter import FilterActivities, FilterGarage, filter_equipment, filter_activities
from show_filtered import ShowFiltered
from add_item import AddItem
import csv
import sys


# TODO ctk.tabview instead of pages
class Main:
    def __init__(self):
        self.__files = ["databases/michele_database.xlsx", "databases/michele_database_backup.xlsx"]
        self.__history = History(self.__files)
        self.__garage = Garage(self.__files)
        self.__medic = MedicalCentre(self.__files)
        self.__garage.update_usage(self.__history)
        self.__run()
        self.__history.write_database()
        self.__garage.write()
        self.__medic.write()

    def __run(self):
        out = {"command": "home", "argument": None}
        while out["command"] != "close":
            # print(out)
            if out["command"] == "close":
                pass

            elif out["command"] == "home":
                m = MenuGUI(self.__garage.get_list(), self.__history.get_list())
                out = m.run()

            elif out["command"] == "add":
                a = AddItem(out["argument"], self.__history.get_list(), self.__garage.get_list())
                out = a.run()
                if out["argument"] is not None:
                    if isinstance(out["argument"], Equipment):
                        print("Saving equip")
                        self.__garage.add_item(out["argument"])
                    elif isinstance(out["argument"], GeneralActivity) or isinstance(out["argument"], DistanceActivity):
                        print("Saving activ")
                        self.__history.add_item(out["argument"])
                    out["argument"] = None

            elif out["command"] == "filter_equipment":
                f = FilterGarage(self.__garage)
                out = f.run()
                if isinstance(out["argument"], dict):
                    items = filter_equipment(self.__garage.get_list(), out["argument"])
                    out = {"command": "navigate", "argument": items}

            elif out["command"] == "filter_activities":
                f = FilterActivities(self.__history)
                out = f.run()
                if isinstance(out["argument"], dict):
                    items = filter_activities(self.__history.get_list(), out["argument"])
                    out = {"command": "navigate", "argument": items}

            elif out["command"] == "navigate":
                s = ShowFiltered(out["argument"], self.__garage, self.__history)
                out = s.run()

            elif out["command"] == "modify":
                out = {"command": "home", "argument": None}
                # m = AddModifyItem(None, out["argument"], self.__history.get_list(), self.__garage.get_list())
                # m.run()

            elif out["command"] == "delete":
                if isinstance(out["argument"], Equipment):
                    self.__garage.remove_item(out["argument"])
                else:
                    self.__history.remove_item(out["argument"])

                out = {"command": "home", "argument": None}


            elif out["command"] == "visualize":
                out = {"command": "home", "argument": None}

            elif out["command"] == "statistics":
                out = {"command": "home", "argument": None}

            elif out["command"] == "continue":
                out = {"command": "home", "argument": None}

            else:
                sys.exit("Unexpected output from the menu")


def main():
    Main()


if __name__ == "__main__":
    main()
