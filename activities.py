import datetime
import sys
import pandas as pd
from utils import *


class History:
    def __init__(self, files):
        self.__file = [files[0], "activities"]  # filename and sheet_name
        self.__backup_file = [files[1], "activities"]  # filename and sheet_name
        self.__available = ["Run", "Bike", "Hike", "Gym", "Sport"]
        self.__stats = {}
        self.__backup(download=True)
        self.__activities_list = self.__read_database()
        self.__update_stats()

    def get_list(self):
        return self.__activities_list

    def get_file(self):
        return self.__file

    def get_available(self):
        return self.__available

    def get_backup_file(self):
        return self.__backup_file

    def add_item(self, item):
        self.__activities_list.append(item)
        self.__update_stats()

    def remove_item(self, item):
        for i, activity in enumerate(self.__activities_list):
            if item == activity:
                self.__activities_list.pop(i)
                break
        self.__update_stats()

    def get_stats(self):
        return self.__stats

    def __update_stats(self):
        item_num = len(self.get_list())
        oldest = min([x.get_date() for x in self.get_list()])
        newest = max([x.get_date() for x in self.get_list()])
        available = self.get_available()
        counter = np.zeros(shape=[5, ])
        total_time = 0
        total_distance = 0
        distance_per_activity = np.zeros([5, ])
        time_per_activity = np.zeros([5, ])
        for item in self.get_list():
            idx = available.index(item.get_item_type())
            counter[idx] += 1
            try:
                total_distance += item.get_distance()
                distance_per_activity[idx] += item.get_distance()
            except AttributeError:
                pass
            try:
                time = item.get_duration()
                total_time += time.get_total_seconds()
                time_per_activity[idx] += time.get_total_seconds()
            except AttributeError:
                pass
        total_time = Duration(total_seconds=total_time)
        how_many = max(counter)
        idx = np.argmax(how_many)
        most_frequent = available[idx]
        print(time_per_activity, distance_per_activity)
        string = f"{item_num} activities, from {oldest} to {newest}\nMost frequent: {most_frequent} ({how_many})\n" \
                 f"Total Distance: {total_distance:.2f}km. \nTotal Time: {total_time}days\n"

        self.__stats = {"num": item_num, "activities_counter": counter, "oldest": oldest, "newest": newest,
                        "most_frequent": most_frequent, "most_frequent_num": how_many, "total_time": total_time,
                        "total_distance": total_distance, "string": string}
        print(string)

    def __read_database(self):
        filename = self.get_file()
        items = []
        reader = pd.read_excel(filename[0], sheet_name=filename[1])
        for _, row in reader.iterrows():
            if row["item_type"] in ["Run", "Hike", "Bike"]:
                item = DistanceActivity(row)
            elif row["item_type"] in ["Sport", "Gym"]:
                item = GeneralActivity(row)
            else:
                sys.exit("Invalid Activity")
            items.append(item)
        return items

    def write_database(self):
        filename = self.get_file()
        file = []
        for item in self.get_list():
            ans = item.write()
            file.append(ans)
        file = pd.DataFrame(file)
        with pd.ExcelWriter(filename[0], mode="a", if_sheet_exists="replace") as writer:
            file.to_excel(writer, sheet_name=filename[1], index=False)
        self.__backup(download=False)

    def __backup(self, download):
        """ download is True if we want to restore the data from the backup file to the actual database
        download is False if we are storing the updated data to the backup, after a successful writing operation"""
        if download:
            source = self.get_backup_file()
            target = self.get_file()
        else:
            source = self.get_file()
            target = self.get_backup_file()

        file = pd.read_excel(source[0], sheet_name=source[1])
        with pd.ExcelWriter(target[0], mode="a", if_sheet_exists="replace") as writer:
            file.to_excel(writer, sheet_name=target[1], index=False)


class DistanceActivity:
    def __init__(self, sequence):
        self.__item_type = ""
        self.__date = None
        self.__distance = None
        self.__duration = None
        self.__typology = None
        self.__max_altitude = None
        self.__altitude_gain = None
        self.__fatigue = None
        self.__equipment = None
        self.__hot = None
        self.__rain = None
        self.__notes = None
        self.__available_typology_list = []
        self.__read(sequence)
        self.__pace = self.__compute_pace()
        
    def __read(self, sequence):
        self.__item_type = sequence["item_type"]
        dd, mm, yy = sequence["date_day":"date_year"]
        self.__date = datetime.date(int(yy), int(mm), int(dd))
        self.__distance = float(sequence["distance"])
        hh, mm, ss = sequence["duration_hour":"duration_second"]
        self.__duration = Duration(hours=int(hh), minutes=int(mm), seconds=int(ss))
        self.__equipment = int(sequence["equipment"])
        self.__typology = sequence["typology"]
        self.__max_altitude = int(sequence["max_altitude"])
        self.__altitude_gain = int(sequence["altitude_gain"])
        self.__fatigue = float(sequence["fatigue"])
        self.__hot = bool(sequence["hot"])
        self.__rain = bool(sequence["rain"])
        self.__notes = sequence["notes"]

        if self.__item_type == "Run":
            self.__available_typology_list = ["Normal", "Distance", "Recovery", "Speed", "Race"]
        elif self.__item_type == "Hike":
            self.__available_typology_list = ["Mountain", "Walk", "Race"]
        elif self.__item_type == "Bike":
            self.__available_typology_list = ["Mountain", "Flat", "Mixed", "Race"]

    def write(self):
        if isinstance(self.__date, datetime.date) and \
                isinstance(self.__distance, float) and \
                isinstance(self.__duration, Duration) and \
                self.__typology in self.__available_typology_list and \
                isinstance(self.__max_altitude, int) and isinstance(self.__altitude_gain, int) and \
                ((isinstance(self.__fatigue, float) and 0.0 <= self.__fatigue <= 10.0) or self.__fatigue is None) and \
                isinstance(self.__equipment, int) and \
                self.__hot in [True, False] and self.__rain in [True, False]:
            ans = {"item_type": self.__item_type, "date_day": self.__date.day, "date_month": self.__date.month,
                   "date_year": self.__date.year, "distance": self.__distance, "duration_hour": self.__duration.hours,
                   "duration_minute": self.__duration.minutes, "duration_second": self.__duration.seconds,
                   "equipment": self.__equipment, "typology": self.__typology, "max_altitude": self.__max_altitude,
                   "altitude_gain": self.__altitude_gain, "fatigue": self.__fatigue, "hot": self.__hot,
                   "rain": self.__rain, "indoor": False, "notes": self.__notes}
        else:
            sys.exit("Error while writing")
        return ans
        
    def __str__(self):
        string = f"Date: {self.__date.day}/{self.__date.month}/{self.__date.year}:\n" \
               f"{self.__item_type}: {self.__distance}km in {self.__duration.minutes + self.__duration.hours*60}min, " \
                 f"{self.__duration.seconds}s\n" \
               f"Pace: {self.__pace}km/h ({self.invert_pace(self.__pace)})\n" \
               f"Max Altitude: {self.__max_altitude}m, Gaining {self.__altitude_gain}m\n"\
               f"Shoes: {self.__equipment}, Typology: {self.__typology}\n" \
               f"Fatigue: {self.__fatigue}\n"
        if self.__rain or self.__hot:
            string += f"In conditions: "
            if self.__rain:
                string += "Rainy"
            if self.__rain and self.__hot:
                string += " and "
            if self.__hot:
                string += "Hot"
            string += f"\nNotes: {self.__notes}\n"
        return string

    def __compute_pace(self):
        elapsed = self.__duration.hours + self.__duration.minutes/60 + self.__duration.seconds/3600
        return self.__distance/elapsed

    def get_date(self):
        return self.__date

    def get_item_type(self):
        return self.__item_type
    
    def get_distance(self):
        return self.__distance
    
    def get_duration(self):
        return self.__duration
    
    def get_pace(self):
        return self.__pace

    def get_equipment(self):
        return self.__equipment

    def get_typology(self):
        return self.__typology

    def get_max_altitude(self):
        return self.__max_altitude

    def get_altitude_gain(self):
        return self.__altitude_gain

    def get_fatigue(self):
        return self.__fatigue

    def get_conditions(self):
        return self.__hot, self.__rain

    def get_notes(self):
        return self.__notes

    @staticmethod
    def invert_pace(pace: float):
        seconds = int(np.floor(3600/pace))  # from km/h to s/km
        time_pace = Duration(total_seconds=seconds)  # use the seconds to initialize a duration object (duration/km)
        return time_pace

    @staticmethod
    def get_available(item_type: str):
        if item_type.title() == "Run":
            return ["Normal", "Distance", "Recovery", "Speed", "Race"]
        elif item_type.title() == "Hike":
            return ["Mountain", "Walk", "Race"]
        elif item_type.title() == "Bike":
            return ["Mountain", "Flat", "Mixed", "Race"]
        else:
            raise ValueError

    @staticmethod
    def get_available_equipment(item_type: str):
        if item_type.title() == "Run":
            return ["Running Shoes"]
        elif item_type.title() == "Hike":
            return ["Walking Shoes", "Hiking Boots", "Running Shoes"]
        elif item_type.title() == "Bike":
            return ["Road Bike", "Mountain Bike"]
        else:
            raise ValueError


class GeneralActivity:
    def __init__(self, sequence):
        self.__item_type = ""
        self.__date = None
        self.__duration = None
        self.__typology = None
        self.__fatigue = None
        self.__equipment = None
        self.__indoor = None
        self.__hot = None
        self.__notes = None
        self.__available_typology_list = []
        self.__read(sequence)

    def __read(self, sequence):
        self.__item_type = sequence["item_type"]
        dd, mm, yy = sequence["date_day":"date_year"]
        self.__date = datetime.date(int(yy), int(mm), int(dd))
        hh, mm, ss = sequence["duration_hour":"duration_second"]
        self.__duration = Duration(hours=int(hh), minutes=int(mm), seconds=int(ss))
        self.__equipment = int(sequence["equipment"])
        self.__typology = sequence["typology"]
        self.__fatigue = float(sequence["fatigue"])
        self.__indoor = bool(sequence["indoor"])
        self.__hot = bool(sequence["hot"])
        self.__notes = sequence["notes"]

        if self.__item_type == "Gym":
            self.__available_typology_list = ["Back", "Legs", "Abs", "Pull", "Push", "Pecs"]
        elif self.__item_type == "Sport":
            self.__available_typology_list = ["Football", "Ski", "Nordic Ski", "Volleyball", "Beach Volley", "Baseball",
                                              "Basketball"]

    def write(self):
        if isinstance(self.__date, datetime.date) and \
                isinstance(self.__duration, Duration) and \
                self.__typology in self.__available_typology_list and \
                ((isinstance(self.__fatigue, float) and 0.0 <= self.__fatigue <= 10.0) or self.__fatigue is None) and \
                self.__indoor in [True, False] and self.__hot in [True, False]:
            ans = {"item_type": self.__item_type, "date_day": self.__date.day, "date_month": self.__date.month,
                   "date_year": self.__date.year, "distance": np.NaN, "duration_hour": self.__duration.hours,
                   "duration_minute": self.__duration.minutes, "duration_second": self.__duration.seconds,
                   "equipment": self.__equipment, "typology": self.__typology, "max_altitude": np.NaN,
                   "altitude_gain": np.NaN, "fatigue": self.__fatigue, "hot": self.__hot, "indoor": self.__indoor,
                   "rain": False, "notes": self.__notes}
        else:
            sys.exit("Error while writing")
        return ans

    def __str__(self):
        string = f"Date: {self.__date.day}/{self.__date.month}/{self.__date.year}:\n" \
               f"{self.__item_type}: {self.__typology} for {self.__duration.hours}h, {self.__duration.minutes}min, " \
                 f"{self.__duration.seconds}s\n" \
               f"Fatigue: {self.__fatigue}\n"
        if self.__indoor:
            string += f"Indoors "
        else:
            string += f"Outdoors "
        if self.__hot:
            string += f"and Hot"
        string += f"\nNotes: {self.__notes}\n"
        return string

    def get_item_type(self):
        return self.__item_type

    def get_date(self):
        return self.__date

    def get_duration(self):
        return self.__duration

    def get_typology(self):
        return self.__typology

    def get_equipment(self):
        return self.__equipment

    def get_fatigue(self):
        return self.__fatigue

    def get_conditions(self):
        return self.__hot, self.__indoor

    def get_notes(self):
        return self.__notes

    @staticmethod
    def get_available(typology: str):
        if typology == "Gym":
            return ["Back", "Legs", "Abs", "Pull", "Push", "Pecs"]
        elif typology == "Sport":
            return ["Football", "Ski", "Nordic Ski", "Volleyball", "Beach Volley", "Baseball", "Basketball"]
        else:
            raise ValueError

    @staticmethod
    def get_available_equipment(typology: str):
        if typology == "Gym":
            return []
        elif typology == "Sport":
            return ["Skis", "Football Shoes", "Running Shoes"]
        else:
            raise ValueError
