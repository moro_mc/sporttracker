import datetime
import numpy as np
import matplotlib.pyplot as plt
from activities import Run, Hike, Bike, Training, Sport
import customtkinter as ctk
import tkinter as tk
from tkinter import messagebox
import sys


def select_show_plot(items):
    if len(items) < 5:
        i = InvalidGUI()
        out = i.run()
        return out
    out = {"command": "select", "argument": None}
    while out["command"] not in ["close", "continue"]:
        if out["command"] == "select":
            s = SelectPlotGUI(items)
            out = s.run()
        elif out["command"] == "show":
            mask = out["argument"]
            if mask["type"] == "pace":
                print("\n\n mo lo faccio \n\n")
                pace_plot(items, mask["options"])
            elif mask["type"] == "distance":
                distance_plot(items, mask["options"])
            elif mask["type"] == "duration":
                duration_plot(items, mask["options"])
            elif mask["type"] == "3d":
                duration_pace_3d_plot(items, mask["options"])
            elif mask["type"] == "monthly":
                monthly_plot(items, mask["options"])
            out = {"command": "select", "argument": None}
    return out


class InvalidGUI:
    def __init__(self):
        self.__out = {"command": "close", "argument": None}
        self.__full_height = 720
        self.__bottom_height = 80
        self.__width = 800
        dims = str(self.__width) + "x" + str(self.__full_height) + "+10+10"

        ctk.set_appearance_mode("system")
        ctk.set_default_color_theme("green")
        self.__window = ctk.CTk()  # create window
        self.__window.geometry(dims)
        self.__window.resizable(False, False)
        self.__window.title("Sport Tracker: Navigate")
        # call the specified function, if the user clicks the red arrow
        self.__window.protocol("WM_DELETE_WINDOW", self.__close)

        self.__main_frame = ctk.CTkFrame(self.__window, width=self.__width,
                                         height=self.__full_height - self.__bottom_height)
        self.__bottom_frame = ctk.CTkFrame(self.__window, width=self.__width, height=self.__bottom_height)
        self.__back_to_menu_button = ctk.CTkButton(self.__bottom_frame, text="Menu", font=("Arial", 20), height=2,
                                                   command=self.__back_to_menu)
        self.__exit_button = ctk.CTkButton(self.__bottom_frame, text="Exit", font=("Arial", 20), height=2,
                                           command=self.__close)
        self.__menu_prompt = ctk.CTkLabel(self.__main_frame, font=("Arial", 24),
                                          text="Error!\nNot enough items to plot! Try removing some of the filters ")

    def run(self):
        self.__assembly()
        self.__window.mainloop()
        return self.__out

    def __assembly(self):
        self.__main_frame.place(x=0, y=0)
        self.__bottom_frame.place(x=0, y=self.__full_height - self.__bottom_height)
        self.__menu_prompt.place(x=20, y=20)
        self.__back_to_menu_button.place(x=60, y=20)
        self.__exit_button.place()

    def __close(self):
        ans = tk.messagebox.askyesno(title="Exit", message="Are you sure you want to quit?")  # create messagebox
        if ans:  # if yes, close
            self.__window.destroy()  # close the home window
            self.__out = {"command": "close", "argument": None}
        else:  # if no, ignore
            pass

    def __back_to_menu(self):
        self.__window.destroy()
        self.__out = {"command": "continue", "argument": None}


class SelectPlotGUI:
    def __init__(self, items):
        self.__out = {"command": "close", "argument": None}
        self.__items = items
        self.__items = sorted(items, key=lambda item: item.get_date())
        for item in self.__items:
            print(item.get_date())
        self.__homogeneous = False
        if all(isinstance(item, Run) for item in self.__items) or \
           all(isinstance(item, Hike) for item in self.__items) or\
           all(isinstance(item, Bike) for item in self.__items) or \
           all(isinstance(item, Training) for item in self.__items) or \
           all(isinstance(item, Sport) for item in self.__items):
            self.__homogeneous = True
        self.__with_distance = True
        if any(isinstance(item, Training) for item in self.__items) or \
                any(isinstance(item, Sport) for item in self.__items):
            self.__with_distance = False

        self.__full_height = 720
        self.__bottom_height = 80
        self.__width = 800
        dims = str(self.__width) + "x" + str(self.__full_height) + "+10+10"

        ctk.set_appearance_mode("system")
        ctk.set_default_color_theme("green")
        self.__window = ctk.CTk()  # create window
        self.__window.geometry(dims)
        self.__window.resizable(False, False)
        self.__window.title("Sport Tracker: Navigate")
        # call the specified function, if the user clicks the red arrow
        self.__window.protocol("WM_DELETE_WINDOW", self.__close)

        self.__main_frame = ctk.CTkFrame(self.__window, width=self.__width,
                                         height=self.__full_height - self.__bottom_height)
        self.__bottom_frame = ctk.CTkFrame(self.__window, width=self.__width, height=self.__bottom_height)
        self.__back_to_menu_button = ctk.CTkButton(self.__bottom_frame, text="Menu", font=("Arial", 20), height=2,
                                                   command=self.__back_to_menu)
        self.__exit_button = ctk.CTkButton(self.__bottom_frame, text="Exit", font=("Arial", 20), height=2,
                                           command=self.__close)
        self.__apply_button = ctk.CTkButton(self.__main_frame, text="Apply", font=("Arial", 20), height=2,
                                            command=self.__apply)
        self.__clear_button = ctk.CTkButton(self.__main_frame, text="Clear", font=("Arial", 20), height=2,
                                            command=self.__clear)

        self.__menu_prompt = ctk.CTkLabel(self.__main_frame, text="Select Plot and its Options", font=("Arial", 24))
        self.__error_label = ctk.CTkLabel(self.__main_frame, text="Select one plot to proceed", font=("Arial", 16),
                                          text_color="red")

        self.__pace_var = ctk.BooleanVar(value=False)
        self.__pace_check = ctk.CTkCheckBox(self.__main_frame, text="Pace in time Plot", font=("Arial", 16),
                                            onvalue=True, offvalue=False, hover=True,
                                            variable=self.__pace_var)

        self.__distance_var = ctk.BooleanVar(value=False)
        self.__distance_check = ctk.CTkCheckBox(self.__main_frame, text="Distance in time Plot", font=("Arial", 16),
                                                onvalue=True, offvalue=False, hover=True,
                                                variable=self.__distance_var, command=self.__distance_opts)
        self.__conditions_var = ctk.BooleanVar(value=False)
        self.__conditions_check = ctk.CTkCheckBox(self.__main_frame, text="Highlight conditions", font=("Arial", 16),
                                                    onvalue=True, offvalue=False, hover=True,
                                                    variable=self.__conditions_var)
        self.__pace_colorbar_var = ctk.BooleanVar(value=False)
        self.__pace_colorbar_check = ctk.CTkCheckBox(self.__main_frame, text="Highlight Pace", font=("Arial", 16),
                                                onvalue=True, offvalue=False, hover=True,
                                                variable=self.__pace_colorbar_var)
        self.__fatigue_colorbar_var = ctk.BooleanVar(value=False)
        self.__fatigue_colorbar_check = ctk.CTkCheckBox(self.__main_frame, text="Highlight Fatigue", font=("Arial", 16),
                                                onvalue=True, offvalue=False, hover=True,
                                                variable=self.__fatigue_colorbar_var)
        self.__altitude_colorbar_var = ctk.BooleanVar(value=False)
        self.__altitude_colorbar_check = ctk.CTkCheckBox(self.__main_frame, text="Highlight Altitude Gain", font=("Arial", 16),
                                                onvalue=True, offvalue=False, hover=True,
                                                variable=self.__altitude_colorbar_var)

        self.__duration_var = ctk.BooleanVar(value=False)
        self.__duration_check = ctk.CTkCheckBox(self.__main_frame, text="Duration in time Plot", font=("Arial", 16),
                                                onvalue=True, offvalue=False, hover=True,
                                                variable=self.__duration_var)

        self.__3d_var = ctk.BooleanVar(value=False)
        self.__3d_check = ctk.CTkCheckBox(self.__main_frame, text="Distance - Duration - Pace Plot", font=("Arial", 16),
                                          onvalue=True, offvalue=False, hover=True,
                                          variable=self.__3d_var)

        self.__monthly_var = ctk.BooleanVar(value=False)
        self.__monthly_check = ctk.CTkCheckBox(self.__main_frame, text="Monthly Progress Plot", font=("Arial", 16),
                                               onvalue=True, offvalue=False, hover=True,
                                               variable=self.__monthly_var)

        self.__options_label = ctk.CTkLabel(self.__main_frame, text="Select Plot Option (select one)", font=("Arial", 20))
        self.__options = [self.__conditions_check, self.__fatigue_colorbar_check, self.__pace_colorbar_check,
                          self.__altitude_colorbar_check]
        self.__plots_checks = [self.__pace_check, self.__distance_check, self.__duration_check, self.__3d_check,
                               self.__monthly_check]
        self.__plots_vars = [self.__pace_var, self.__distance_var, self.__duration_var, self.__3d_var,
                             self.__monthly_var]

    def run(self):
        self.__assembly()
        self.__window.mainloop()
        return self.__out

    def __assembly(self):
        self.__bottom_frame.place(x=0, y=self.__full_height - self.__bottom_height)
        self.__back_to_menu_button.place(x=60, y=20)
        self.__exit_button.place(x=580, y=20)
        self.__main_frame.place(x=0, y=0)
        self.__menu_prompt.place(x=20, y=20)
        self.__apply_button.place(x=60, y=self.__full_height - self.__bottom_height - 40)
        self.__clear_button.place(x=580, y=self.__full_height - self.__bottom_height - 40)
        self.__duration_check.place(x=30, y=100)
        if self.__homogeneous:
            if self.__with_distance:
                self.__pace_check.place(x=30, y=180)
                self.__distance_check.place(x=30, y=140)
                self.__3d_check.place(x=30, y=220)
                self.__monthly_check.place(x=30, y=260)

    def __close(self):
        ans = tk.messagebox.askyesno(title="Exit", message="Are you sure you want to quit?")  # create messagebox
        if ans:  # if yes, close
            self.__window.destroy()  # close the home window
            self.__out = {"command": "close", "argument": None}
        else:  # if no, ignore
            pass

    def __back_to_menu(self):
        self.__window.destroy()
        self.__out = {"command": "continue", "argument": None}

    def __apply(self):
        selected = 0
        for item in self.__plots_vars:
            if item.get():
                selected += 1
        if selected != 1:
            # self.__error_label.configure(text="No item to select")
            self.__error_label.place(x=40, y=550)
            return
        else:
            if self.__pace_var.get():
                mask = {"type": "pace"}
            elif self.__distance_var.get():
                mask = self.__distance_apply()
            elif self.__duration_var.get():
                mask = {"type": "duration"}
            elif self.__3d_var.get():
                mask = {"type": "3d"}
            elif self.__monthly_var.get():
                mask = {"type": "monthly"}
            else:
                sys.exit("Unexpected error in PLOT SELECTION")
            if mask is not None:
                self.__out = {"command": "show", "argument": mask}
                self.__window.destroy()
            return

    def __clear(self):
        self.__options_label.place_forget()
        for feature in self.__options:
            feature.deselect()
            feature.place_forget()
        for feature in self.__plots_checks:
            feature.deselect()

    def __distance_opts(self):
        self.__error_label.place_forget()
        self.__options_label.place(x=400, y=100)
        # for feature in self.__plots_checks:
        #     feature.deselect()
        # self.__distance_check.select()
        for feature in self.__options:
            feature.deselect()
            feature.place_forget()
        self.__conditions_check.place(x=400, y=160)
        self.__fatigue_colorbar_check.place(x=400, y=200)
        self.__pace_colorbar_check.place(x=400, y=240)
        self.__altitude_colorbar_check.place(x=400, y=280)

    def __distance_apply(self):
        selected = 0
        for feature in [self.__conditions_var, self.__fatigue_colorbar_var, self.__altitude_colorbar_var,
                        self.__pace_colorbar_var]:
            if feature.get():
                selected += 1
        if selected > 1:
            self.__error_label.configure(text="Select at most one option")
            self.__error_label.place(x=40, y=550)
            mask = None
        else:
            if self.__conditions_var.get():
                mask = {"type": "distance", "options": "conditions"}
            elif self.__fatigue_colorbar_var.get():
                mask = {"type": "distance", "options": "fatigue"}
            elif self.__pace_colorbar_var.get():
                mask = {"type": "distance", "options": "pace"}
            elif self.__altitude_colorbar_var.get():
                mask = {"type": "distance", "options": "gain"}
            else:
                mask = {"type": "distance", "options": "none"}
        return mask


def pace_plot(items, options):
    pass
    # x_vec = np.arange(len(items))
    # x_ticks = []
    # y_vec = []
    # y_ticks = []
    # for item in items:
    #     x_ticks.append(f"{item.get_date().day}-{item.get_date().month}-{item.get_date().year}")
    #     y_vec.append(item.get_pace().hour*60 + item.get_pace().minute + item.get_pace().second/60)
    #     y_ticks.append(f"{item.get_pace().hour*60 + item.get_pace().minute}:{item.get_pace().second}")
    # plt.figure()
    # plt.plot(x_vec, y_vec, color="k")
    # plt.xlabel("Date")
    # plt.ylabel("Pace [min/km]")
    # plt.xticks(x_vec, x_ticks)
    # plt.yticks(y_vec, y_ticks)
    # plt.grid(True)
    # plt.show()


def duration_plot(items, options):
    pass
    # duration_vec = []
    # x_vec = np.arange(len(items))
    # x_ticks = []
    # y_vec = []
    # y_ticks = []
    # y_ticks_names = []
    # for item in items:
    #     x_ticks.append(f"{item.get_date().day}-{item.get_date().month}-{item.get_date().year}")
    #     duration_vec.append(item.get_duration())
    #     y_vec.append(item.get_duration().hour*60 + item.get_duration().minute + item.get_duration().second/60)
    # y_min = min(duration_vec)
    # y_max = max(duration_vec)
    # delta = datetime.datetime.combine(datetime.date.today(), y_max) - datetime.datetime.combine(datetime.date.today(), y_min)
    # y_min = datetime.datetime.combine(datetime.date.today(), y_min)
    # print(delta)
    # for i in np.arange(-1,12):
    #     y = y_min + i*delta/10
    #     y_ticks.append(y.hour*60 + y.minute + y.second/60)
    #     y_ticks_names.append(f"{y.hour*60 + y.minute:2d}:{y.second:2d}")
    # print(y_ticks)
    # print(y_ticks_names)
    #
    # plt.figure()
    # plt.plot(x_vec, y_vec, color="k")
    # plt.xlabel("Date")
    # plt.ylabel("Duration [min]")
    # plt.xticks(x_vec, x_ticks)
    # plt.yticks(y_ticks, y_ticks_names)
    # plt.grid(True)
    # plt.show()


def distance_plot(items, options):
    x_vec = np.arange(len(items))
    x_ticks = []
    y_vec = []
    c_vec = []
    for item in items:
        x_ticks.append(f"{item.get_date().day}-{item.get_date().month}")#-{item.get_date().year}")
        y_vec.append(item.get_distance())
        if options == "fatigue":
            c_vec.append(item.get_fatigue())
            v_min = 0
            v_max = 10
        elif options == "pace":
            if isinstance(item, Bike):
                c_vec.append(item.get_pace())
            else:
                ans = item.get_pace()
                c_vec.append(ans.hour*60 + ans.minute + ans.second/60)
            v_min = min(c_vec)
            v_max = max(c_vec)
        elif options == "gain":
            c_vec.append(item.get_altitude_gain())
            v_min = min(c_vec)
            v_max = max(c_vec)
        else:
            c_vec.append(0)
            v_min = 0
            v_max = 1
    y_ticks = np.linspace(np.floor(0.8*min(y_vec)), np.floor(1.2*max(y_vec)), 10)
    print(c_vec)

    plt.figure()
    plt.plot(x_vec, y_vec, color="k")
    plt.scatter(x_vec, y_vec, c=c_vec, vmin=v_min, vmax=v_max)
    if options != "none":
        plt.colorbar()
    plt.xlabel("Date")
    plt.ylabel("Distance [km]")
    plt.xticks(x_vec, x_ticks)
    plt.yticks(y_ticks)
    plt.grid(True)
    plt.show()


def duration_pace_3d_plot(items, options):
    pass
    # ax = plt.figure().add_subplot(projection='3d')


def monthly_plot(items, options):
    pass
    # todo sum up month by month, duration and distance, plot a duration-distance plot, month by month
