import sys
import re
import numpy as np
import pandas as pd
import datetime
import tkinter as tk
from tkinter import ttk, messagebox
import customtkinter as ctk
from activities import DistanceActivity, GeneralActivity
from equipment import Equipment
from utils import *


# TODO add a separator and an "optional" label to highlight what is not mandatory
class AddItem:
    def __init__(self, typology, activities, garage):
        self.__out = {"command": "close", "argument": None}
        self.__item_typology = typology
        self.__activities = activities
        self.__garage = garage

        self.__date = None
        self.__duration = None
        self.__distance = None
        self.__max_altitude = None
        self.__altitude_gain = None
        self.__equipment = None
        self.__fatigue = None
        self.__hot = None
        self.__rain = None
        self.__indoor = None
        self.__typology = None

        self.__id = None
        self.__notes = None
        self.__alive = None

        self.__full_height = 720
        self.__width = 800
        dims = str(self.__width) + "x" + str(self.__full_height) + "+10+10"

        ctk.set_appearance_mode("system")
        ctk.set_default_color_theme("green")
        self.__window = ctk.CTk()  # create window
        self.__window.geometry(dims)
        self.__window.resizable(False, False)
        self.__window.title("Sport Tracker: Navigate History")
        # call the specified function, if the user clicks the red arrow
        self.__window.protocol("WM_DELETE_WINDOW", self.__close)

        self.__bottom_frame = ctk.CTkFrame(self.__window)
        self.__back_to_menu_button = ctk.CTkButton(self.__bottom_frame, text="Menu", font=("Arial", 20), height=2,
                                                   command=self.__back_to_menu)
        self.__exit_button = ctk.CTkButton(self.__bottom_frame, text="Exit", font=("Arial", 20), height=2,
                                           command=self.__close)

        self.__action_frame = ctk.CTkFrame(self.__window)
        self.__apply_button = ctk.CTkButton(self.__action_frame, text="Apply", font=("Arial", 20), height=2,
                                            command=self.__apply)
        self.__clear_button = ctk.CTkButton(self.__action_frame, text="Clear", font=("Arial", 20), height=2,
                                            command=self.__clear)

        self.__main_frame = ctk.CTkFrame(self.__window)

        self.__menu_prompt = ctk.CTkLabel(self.__main_frame, text="Insert Data", font=("Arial", 24))

        self.__typology_frame = ctk.CTkFrame(self.__main_frame, fg_color=self.__main_frame._fg_color)
        self.__typology_label = ctk.CTkLabel(self.__typology_frame, text="Typology:      ", font=("Arial", 18))
        available_activities = self.__available()
        self.__typology_combo = ctk.CTkComboBox(self.__typology_frame, values=available_activities, hover=True)

        self.__date_frame = ctk.CTkFrame(self.__main_frame, fg_color=self.__main_frame._fg_color)
        self.__date_label = ctk.CTkLabel(self.__date_frame, text="Date:      ", font=("Arial", 18))
        self.__date_dd = ctk.CTkEntry(self.__date_frame, width=40, placeholder_text="dd")
        self.__date_slash1 = ctk.CTkLabel(self.__date_frame, text="/", font=("Arial", 18))
        self.__date_mm = ctk.CTkEntry(self.__date_frame, width=40, placeholder_text="mm")
        self.__date_slash2 = ctk.CTkLabel(self.__date_frame, text="/", font=("Arial", 18))
        self.__date_yy = ctk.CTkEntry(self.__date_frame, width=60, placeholder_text="yyyy")

        self.__distance_frame = ctk.CTkFrame(self.__main_frame, fg_color=self.__main_frame._fg_color)
        self.__distance_label = ctk.CTkLabel(self.__distance_frame, text="Distance [km]:      ", font=("Arial", 18))
        self.__distance_entry = ctk.CTkEntry(self.__distance_frame, width=40, placeholder_text="0.0")

        self.__duration_frame = ctk.CTkFrame(self.__main_frame, fg_color=self.__main_frame._fg_color)
        self.__duration_label = ctk.CTkLabel(self.__duration_frame, text="Duration:      ", font=("Arial", 18))
        self.__duration_hh = ctk.CTkEntry(self.__duration_frame, width=40, placeholder_text="hh")
        self.__duration_slash1 = ctk.CTkLabel(self.__duration_frame, text=":", font=("Arial", 18))
        self.__duration_mm = ctk.CTkEntry(self.__duration_frame, width=40, placeholder_text="mm")
        self.__duration_slash2 = ctk.CTkLabel(self.__duration_frame, text=":", font=("Arial", 18))
        self.__duration_ss = ctk.CTkEntry(self.__duration_frame, width=40, placeholder_text="ss")

        self.__max_altitude_frame = ctk.CTkFrame(self.__main_frame, fg_color=self.__main_frame._fg_color)
        self.__max_altitude_label = ctk.CTkLabel(self.__max_altitude_frame, text="Maximum Altitude [m]:      ",
                                                 font=("Arial", 18))
        self.__max_altitude_entry = ctk.CTkEntry(self.__max_altitude_frame, width=40, placeholder_text="0.0")

        self.__altitude_gain_frame = ctk.CTkFrame(self.__main_frame, fg_color=self.__main_frame._fg_color)
        self.__altitude_gain_label = ctk.CTkLabel(self.__altitude_gain_frame, text="Altitude Gained [m]:      ",
                                                  font=("Arial", 18))
        self.__altitude_gain_entry = ctk.CTkEntry(self.__altitude_gain_frame, width=40, placeholder_text="0.0")

        self.__equipment_frame = ctk.CTkFrame(self.__main_frame, fg_color=self.__main_frame._fg_color)
        self.__equipment_label = ctk.CTkLabel(self.__equipment_frame, text="Equipment ID:      ", font=("Arial", 18))
        available_equipment = self.__filter_equipment()
        self.__equipment_combo = ctk.CTkComboBox(self.__equipment_frame, values=available_equipment, hover=True,
                                                 width=400)

        self.__fatigue_frame = ctk.CTkFrame(self.__main_frame, fg_color=self.__main_frame._fg_color)
        self.__fatigue_label = ctk.CTkLabel(self.__fatigue_frame, text="Fatigue (0-10):      ", font=("Arial", 18))
        self.__fatigue_entry = ctk.CTkEntry(self.__fatigue_frame, width=40, placeholder_text="0.0")

        self.__conditions_frame = ctk.CTkFrame(self.__main_frame, fg_color=self.__main_frame._fg_color)
        self.__conditions_label = ctk.CTkLabel(self.__conditions_frame, text="Conditions:      ", font=("Arial", 18))
        self.__hot_var = ctk.BooleanVar(value=False)
        self.__hot_check = ctk.CTkCheckBox(self.__conditions_frame, text="Hot", font=("Arial", 16),
                                           onvalue=True, offvalue=False, hover=True, variable=self.__hot_var)
        self.__rain_var = ctk.BooleanVar(value=False)
        self.__rain_check = ctk.CTkCheckBox(self.__conditions_frame, text="Rain", font=("Arial", 16),
                                            onvalue=True, offvalue=False, hover=True, variable=self.__rain_var)
        self.__indoor_var = ctk.BooleanVar(value=False)
        self.__indoor_check = ctk.CTkCheckBox(self.__conditions_frame, text="Indoor", font=("Arial", 16),
                                              onvalue=True, offvalue=False, hover=True, variable=self.__indoor_var)

        self.__dead_frame = ctk.CTkFrame(self.__main_frame, fg_color=self.__main_frame._fg_color)
        self.__dead_label = ctk.CTkLabel(self.__dead_frame, text="Not Usable Anymore:      ", font=("Arial", 18))
        self.__dead_var = ctk.BooleanVar(value=False)
        self.__dead_check = ctk.CTkCheckBox(self.__dead_frame, text="", font=("Arial", 16),
                                            onvalue=True, offvalue=False, hover=True, variable=self.__dead_var)

        self.__notes_label = ctk.CTkLabel(self.__main_frame, text="Personal Notes:", font=("Arial", 18))
        self.__notes_entry = ctk.CTkTextbox(self.__main_frame, width=400, height=100)

        self.__error_label = ctk.CTkLabel(self.__main_frame, text="", font=("Arial", 18), text_color="red")

        self.__separator1 = ttk.Separator(self.__bottom_frame, orient="horizontal")
        self.__separator2 = ttk.Separator(self.__action_frame, orient="horizontal")
        self.__optional_label = ctk.CTkLabel(self.__main_frame, text="Optional", font=("Arial", 12))
        self.__separator3 = ttk.Separator(self.__main_frame, orient="horizontal")

        self.__to_clear = [self.__date_dd, self.__date_mm, self.__date_yy, self.__duration_hh, self.__duration_mm,
                           self.__duration_ss, self.__distance_entry, self.__equipment_combo, self.__fatigue_entry,
                           self.__rain_check, self.__hot_check, self.__dead_check, self.__indoor_check,
                           self.__equipment_combo, self.__altitude_gain_entry, self.__max_altitude_entry]

    def __available(self):
        if self.__item_typology in ["Run", "Hike", "Bike"]:
            return DistanceActivity.get_available(self.__item_typology)
        elif self.__item_typology in ["Sport", "Gym"]:
            return GeneralActivity.get_available(self.__item_typology)
        elif self.__item_typology == "Equipment":
            return Equipment.get_available()
        else:
            raise ValueError

    def __filter_equipment(self):
        items = ["None"]
        if self.__item_typology in ["Run", "Hike", "Bike"]:
            for item in self.__garage:
                if item.get_typology() in DistanceActivity.get_available_equipment(self.__item_typology):
                    txt = f"(ID: {item.get_id()}) {item.get_typology()} ({item.get_notes()})"
                    if not item.get_alive():
                        txt += "(Not Usable)"
                    items.append(txt)
        elif self.__item_typology in ["Sport", "Gym"]:
            for item in self.__garage:
                if item.get_typology() in GeneralActivity.get_available_equipment(self.__item_typology):
                    txt = f"(ID: {item.get_id()}) {item.get_typology()} ({item.get_notes()})"
                    if not item.get_alive():
                        txt += "(Not Usable)"
                    items.append(txt)
        elif self.__item_typology == "Equipment":
            pass
        if len(items) == 0:
            items.append("")
        return items

    def run(self):
        self.__assembly()
        self.__window.mainloop()
        return self.__out

    def __assembly(self):
        self.__main_frame.pack(side="top", expand=True, fill="both")
        self.__bottom_frame.pack(side="bottom", expand=False, fill="both")
        self.__action_frame.pack(side="bottom", expand=False, fill="both")
        self.__separator1.pack(side="top", expand=False, fill="both")
        self.__separator2.pack(side="top", expand=False, fill="both")
        self.__apply_button.pack(side="left", expand=False, padx=40, pady=20)
        self.__clear_button.pack(side="right", expand=False, padx=40, pady=20)
        self.__back_to_menu_button.pack(side="left", expand=False, padx=40, pady=20)
        self.__exit_button.pack(side="right", expand=False, padx=40, pady=20)

        ################################################################################################################
        self.__typology_label.pack(side="left")
        self.__typology_combo.pack(side="left")
        self.__date_label.pack(side="left")
        self.__date_dd.pack(side="left")
        self.__date_slash1.pack(side="left", padx=5)
        self.__date_mm.pack(side="left")
        self.__date_slash2.pack(side="left", padx=5)
        self.__date_yy.pack(side="left")

        self.__distance_label.pack(side="left")
        self.__distance_entry.pack(side="left")

        self.__duration_label.pack(side="left")
        self.__duration_hh.pack(side="left")
        self.__duration_slash1.pack(side="left", padx=5)
        self.__duration_mm.pack(side="left")
        self.__duration_slash2.pack(side="left", padx=5)
        self.__duration_ss.pack(side="left")

        self.__max_altitude_label.pack(side="left")
        self.__max_altitude_entry.pack(side="left", padx=5)

        self.__altitude_gain_label.pack(side="left")
        self.__altitude_gain_entry.pack(side="left", padx=5)

        self.__equipment_label.pack(side="left")
        self.__equipment_combo.pack(side="left", padx=5)

        self.__fatigue_label.pack(side="left")
        self.__fatigue_entry.pack(side="left", padx=5)

        self.__conditions_label.pack(side="left")

        self.__dead_label.pack(side="left")
        self.__dead_check.pack(side="left", padx=5)
        ################################################################################################################
        self.__main_frame.columnconfigure(index=0, weight=1)
        self.__main_frame.columnconfigure(index=1, weight=1)
        self.__main_frame.columnconfigure(index=2, weight=1)
        self.__main_frame.columnconfigure(index=3, weight=1)

        self.__menu_prompt.grid(row=0, column=0, sticky="w", padx=20, pady=20, columnspan=4)
        self.__typology_frame.grid(row=1, column=0, sticky="w", padx=20, pady=10, columnspan=1)
        if self.__item_typology == "Equipment":
            self.__dead_frame.grid(row=2, column=0, sticky="w", padx=20, pady=10, columnspan=1)
        else:
            self.__date_frame.grid(row=2, column=0, sticky="w", padx=20, pady=10, columnspan=1)

            if self.__item_typology == "Run" or self.__item_typology == "Hike" or self.__item_typology == "Bike":
                self.__distance_frame.grid(row=2, column=2, sticky="w", padx=20, pady=10, columnspan=1)

            self.__duration_frame.grid(row=4, column=0, sticky="w", padx=20, pady=10, columnspan=1)
            if self.__item_typology == "Run" or self.__item_typology == "Hike" or self.__item_typology == "Bike":
                self.__max_altitude_frame.grid(row=8, column=0, sticky="w", padx=20, pady=10, columnspan=1)
                self.__altitude_gain_frame.grid(row=8, column=2, sticky="w", padx=20, pady=10, columnspan=1)

            self.__equipment_frame.grid(row=7, column=0, sticky="w", padx=20, pady=10, columnspan=4)

            self.__fatigue_frame.grid(row=9, column=0, sticky="w", padx=20, pady=10, columnspan=1)

            if self.__item_typology == "Run" or self.__item_typology == "Hike" or self.__item_typology == "Bike":
                self.__rain_check.pack(side="left", padx=20)
            else:
                self.__indoor_check.pack(side="left", padx=20)
            self.__hot_check.pack(side="left", padx=20)
            self.__conditions_frame.grid(row=9, column=2, sticky="w", padx=20, pady=10, columnspan=1)

        self.__separator3.grid(row=5, column=0, columnspan=4, sticky="ew")
        self.__optional_label.grid(row=6, column=0, sticky="w", padx=10, pady=5)
        self.__notes_label.grid(row=10, column=0, sticky="w", padx=20, pady=10, columnspan=1)
        self.__notes_entry.grid(row=11, column=0, sticky="w", padx=20, pady=10, columnspan=5)

    def __apply(self):
        if self.__item_typology == "Equipment":
            current_ids = []
            for item in self.__garage:
                current_ids.append(item.get_id())
            try:
                self.__id = max(current_ids) + 1
            except ValueError:
                self.__id = 0

            self.__typology = self.__typology_combo.get()
            alive = not self.__dead_var.get()
            self.__alive = alive#f"{alive}"
            self.__notes = self.__notes_entry.get(0.0, ctk.END).strip()
        else:
            # ---- date ----
            try:
                dd = int(self.__date_dd.get())
                mm = int(self.__date_mm.get())
                yy = int(self.__date_yy.get())
                self.__date = datetime.date(yy, mm, dd)
            except ValueError:
                pass
            if self.__date is None:
                self.__display_error("date")
                return
            # ---- typology ----
            try:
                self.__typology = self.__typology_combo.get()
            except ValueError:
                self.__display_error("activity type")
                return
            # ---- duration ----
            try:
                hh = int(self.__duration_hh.get())
                mm = int(self.__duration_mm.get())
                ss = int(self.__duration_ss.get())
                self.__duration = Duration(hours=hh, minutes=mm, seconds=ss)
            except ValueError:
                pass
            if self.__duration is None or self.__duration.hours > 12:
                self.__display_error("duration")
                return
            # ---- equipment ----
            try:
                txt = self.__equipment_combo.get()
                x = re.search(r"(?<=ID: )[\d]", txt)
                if x is None:
                    equipment = 0
                else:
                    equipment = x.group()[0]
                self.__equipment = int(equipment)
            except ValueError:
                self.__display_error("equipment")
            # ---- fatigue ----
            try:
                self.__fatigue = float(self.__fatigue_entry.get())
            except ValueError:
                self.__fatigue = 0.0
            if self.__fatigue < 0:
                self.__fatigue = 0
            elif self.__fatigue > 10:
                self.__fatigue = 10.0

            # ---- hot ----
            try:
                self.__hot = self.__hot_var.get()
            except ValueError:
                self.__hot = False

            # ---- notes ----
            try:
                self.__notes = self.__notes_entry.get(0.0, ctk.END).strip()
            except ValueError:
                self.__notes = ""

            if self.__item_typology in ["Run", "Hike", "Bike"]:
                # ---- distance ----
                try:
                    self.__distance = self.__distance_entry.get()
                except ValueError:
                    self.__display_error("distance")
                    return

                # ---- altitude ----
                try:
                    self.__max_altitude = int(self.__max_altitude_entry.get())
                except ValueError:
                    self.__max_altitude = 0
                # ---- gain ----
                try:
                    self.__altitude_gain = int(self.__altitude_gain_entry.get())
                except ValueError:
                    self.__altitude_gain = 0

                # ---- rain ----
                try:
                    self.__rain = self.__rain_var.get()
                except ValueError:
                    self.__rain = False

            elif self.__item_typology in ["Gym", "Sport"]:
                # ---- indoor ----
                try:
                    self.__indoor = self.__indoor_var.get()
                except ValueError:
                    self.__indoor = False
            else:
                sys.exit("Unexpected typology in ADD")

        self.__window.destroy()
        self.__item = self.__create_item()
        self.__out = {"command": "home", "argument": self.__item}

    def __create_item(self):
        if self.__item_typology == "Equipment":
            sequence = {"ID": self.__id, "typology": self.__typology, "alive": self.__alive, "notes": self.__notes}
            sequence = pd.Series(sequence)
            item = Equipment(sequence)
        else:
            if self.__item_typology in ["Run", "Hike", "Bike"]:
                sequence = {"item_type": self.__item_typology, "date_day": self.__date.day,
                            "date_month": self.__date.month, "date_year": self.__date.year, "distance": self.__distance,
                            "duration_hour": self.__duration.hours, "duration_minute": self.__duration.minutes,
                            "duration_second": self.__duration.seconds, "equipment": self.__equipment,
                            "typology": self.__typology, "max_altitude": self.__max_altitude,
                            "altitude_gain": self.__altitude_gain, "fatigue": self.__fatigue, "hot": self.__hot,
                            "indoor": np.NaN, "rain": self.__rain, "notes": self.__notes}
                sequence = pd.Series(sequence)
                item = DistanceActivity(sequence)
            elif self.__item_typology in ["Gym", "Sport"]:
                sequence = {"item_type": self.__item_typology, "date_day": self.__date.day,
                            "date_month": self.__date.month, "date_year": self.__date.year, "distance": np.NaN,
                            "duration_hour": self.__duration.hours, "duration_minute": self.__duration.minutes,
                            "duration_second": self.__duration.seconds, "equipment": self.__equipment,
                            "typology": self.__typology, "max_altitude": np.NaN, "altitude_gain": np.NaN,
                            "fatigue": self.__fatigue, "hot": self.__hot, "indoor": self.__indoor, "rain": np.NaN,
                            "notes": self.__notes}
                sequence = pd.Series(sequence)
                item = GeneralActivity(sequence)
            else:
                sys.exit("Unexpected typology in ADD")

        return item

    def __display_error(self, msg):
        self.__error_label.configure(text=f"Invalid input: invalid {msg}")
        self.__error_label.grid(row=20, column=1, padx=0, columnspan=3)

    def __clear(self):
        for item in self.__to_clear:
            try:
                item.deselect()
            except AttributeError:
                pass
            try:
                item.delete(0, tk.END)
            except AttributeError:
                pass
        self.__notes_entry.delete(1.0, tk.END)

    def __back_to_menu(self):
        self.__window.destroy()
        self.__out = {"command": "home", "argument": None}

    def __close(self):
        ans = tk.messagebox.askyesno(title="Exit", message="Are you sure you want to quit?")  # create messagebox
        if ans:  # if yes, close
            self.__window.destroy()  # close the home window
            self.__out = {"command": "close", "argument": None}
        else:  # if no, ignore
            pass
        # self.window.destroy()
